# This file is part of PECMAN

# PECMAN is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import cmath
from abc import ABCMeta, abstractmethod

# eigenvalue calulator class
class Experiment(object):
    __metaclass__ = ABCMeta

    def __init__(self,fermi_energy=0.,workfunction=5.):
        try:
            self.fermi_energy = fermi_energy
            self.workfunction = workfunction
        except:
            print("Invalid workfunction value!")

    @property
    @abstractmethod
    def exptype(self):
        raise NotImplementedError("Your Experiment has to define an attribute 'exptype'! ")

    @abstractmethod
    def pes(self,kx,ky,w):
        """Return the photoemission signal"""
        raise NotImplementedError("Should implement pes()!")

    @abstractmethod
    def params_str(self):
        raise NotImplementedError("Should implement params_str()")

    @property
    def fermi_energy(self):
        return self._fermi_energy

    @fermi_energy.setter
    def fermi_energy(self, fermi_energy):
        self._fermi_energy = fermi_energy

    @property
    def workfunction(self):
        return self._workfunction

    @workfunction.setter
    def workfunction(self, workfunction):
        if workfunction < 0: raise ValueError("Invalid value, workfunction cannot be negative!")
        self._workfunction = workfunction
