# This file is part of PECMAN

# PECMAN is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import os
from scipy.integrate import quad

from pecman.experiment import Experiment
from pecman.phelper import *

class Arpes(Experiment):

    # experiment identifyer
    exptype = "arpes"


    def __init__(self,spectral_function,final_state,light_source,
                 fermi_energy=0,workfunction=4.5):
        """
        Create an object for describing an angle-resolved 1PPE experiment
        j1ppe ~ Grf Gaf G< A_L ~ final_state*initial_state*light_source

        Attributes:
        -----------------------------------------------------------------------
        spectral_function
        (SpectralFunction):     representation of the initial state entering
                                via G<
        final_state
        (FinalState):           representation of the final state entering via
                                Gr*Ga, e.g. a ILEEDState,...
        light_source
        (LightSource):          CWSource, Laser, ...
        fermi_energy (float):   Fermi energy of the system [eV]. If set to 0,
                                every binding energy is relative to Ef
        workfunction (float):   The workfunction of the material [eV]
        """
        #self.fermi_energy = fermi_energy
        #self.workfunction = workfunction
        Experiment.__init__(self,fermi_energy,workfunction)
        #self.alpha =  0.0381 #hbar*hbar/2m_electron, [eV nm**2]
        self.spectral_function = spectral_function
        self.final_state = final_state
        self.light_source = light_source



    """
    ===========================================================================
                                PHOTOEMISSION SIGNAL
    ===========================================================================
    """

    def pes_kernel(self,x,kx,ky,w):
        """
        Return the integration kernel of the photoemission signal for
        method = 'em_conserved'. If the light source has a spectral width,
        the spectral function is convoluted with the light source

        Keyword arguments:
        -----------------------------------------------------------------------
        x (float):          photon energy in [eV]
        kx,ky (float):      parallel momentum components [nm^-1]
        w (float):          kinetic energy [eV]
        """
        #integral kernel dx ~ Efield(ekin-x)*Asf(kx,ky,kz,x) dx

        # perpenticular wavevector inside the solid
        kg = np.real(self.final_state.get_ks(kx,ky,w))
        kgi = np.imag(self.final_state.get_ks(kx,ky,w))

        # perpendicular wavevector inside the vacuum
        kd = np.real(self.final_state.get_kv(kx,ky,w))

        # free electron cone * fermi_function * positive kinetic energy
        f0 = th(w-alpha*(kx**2+ky**2))*th( -self.workfunction - x)*th(w)

        # preliminary final state imaginary part of the perpendicular momentum
        k2 = 0.1+kgi
        # final contribution (Gr*Ga)
        f1 = kd/((kd+kg)**2+k2**2)*1./(2.*k2)

        # contribution of the light source
        f2 = self.light_source.value(w-x)

        # photohole contribution = spectral function: evaluated at k-perpendicular of the final state
        # inside the solid
        f3 = self.spectral_function.value(kx,ky,kg,x+self.fermi_energy+self.workfunction)

        kernel = f0*f1*np.abs(f2)**2*f3
        return np.real(kernel)


    def pes_kernel_surface_scat(self,x,kx,ky,qx,qy,w):
        """
        Return the integration kernel of the photoemission signal for
        method = 'em_conserved' with surface scattering. If the light source
        has a spectral width, the spectral function is convoluted with the
        light source

        Keyword arguments:
        -----------------------------------------------------------------------
        x (float):          photon energy [eV]
        kx,ky (float):      parallel momentum components [nm^-1]
        qx,qy (float):      scattering momentum components of the superlattice
                            [nm^-1]
        w (float):          kinetic energy [eV]
        """
        # perpenticular wavevector inside the solid
        kg = np.real(self.final_state.get_ks(kx,ky,w))
        kgi = np.imag(self.final_state.get_ks(kx,ky,w))

        # perpendicular wavevector inside the vacuum
        kd = np.real(self.final_state.get_kv(kx,ky,w))

        # perpenticular wavevector inside the solid before surface scattering
        kmg = np.real(self.final_state.get_ks(kx-qx,ky-qy,w))
        kmgi = np.imag(self.final_state.get_ks(kx-qx,ky-qy,w))

        # free electron cone * fermi_function * positive kinetic energy
        f0 = th(w-alpha*(kx**2+ky**2))*th( -self.workfunction - x)*th(w)

        # preliminary final state imaginary part of the perpendicular momentum
        k2 = 0.1+kgi
        # final contribution (Gr*Ga)
        f1 = kd/((kd+kg)**2+k2**2)*1./(2.*k2)

        # contribution of the light source
        f2 = self.light_source.value(w-x)

        # photohole contribution = spectral function: evaluated at k-perpendicular of the final state before surface scattering
        # inside the solid

        f3 = self.spectral_function.value(kx-qx,ky-qy,kmg,x+self.fermi_energy+self.workfunction)

        kernel = f0*f1*np.abs(f2)**2*f3
        return np.real(kernel)



    def pes_kernel_kz_integrated(self,x,kx,ky,kz,w):
        """
        Return the integration kernel of the photoemission signal for
        method = 'kz_integrated'. If the light source has a spectral width,
        the spectral function is convoluted with the light source

        Keyword arguments:
        -----------------------------------------------------------------------
        x (float):          photon energy in [eV]
        kx,ky (float):      parallel momentum components [nm^-1]
        kz (float):         perpenticular momentum component [nm^-1]
        w (float):          kinetic energy [eV]
        """
        #integral kernel dx ~ Efield(ekin-x)*Asf(kx,ky,kz,x) dx

        # perpenticular wavevector inside the solid
        kg = np.real(self.final_state.get_ks(kx,ky,w))
        kgi = np.imag(self.final_state.get_ks(kx,ky,w))

        # perpendicular wavevector inside the vacuum
        kd = np.real(self.final_state.get_kv(kx,ky,w))

        # free electron cone * fermi_function * positive kinetic energy
        f0 = th(w-alpha*(kx**2+ky**2))*th( -self.workfunction - x)*th(w)

        # preliminary final state imaginary part of the perpendicular momentum
        k2 = 0.1+kgi
        # final contribution (Gr*Ga)
        f1 = kd/((kd+kg)**2+k2**2)*1./(2.*k2)

        # contribution of the light source
        f2 = self.light_source.value(w-x)

        # photohole contribution = spectral function: evaluated at k-perpendicular of the final state
        # inside the solid
        # note: the spectral function is evaluated at kz, not at kg!!
        f3 = self.spectral_function.value(kx,ky,kz,x+self.fermi_energy+self.workfunction)

        # kz-kernel

        kernel = f0*f1*np.abs(f2)**2*f3
        return np.real(kernel)

    def kz_kernel(self,kz,kx,ky,w):
        """
        Calculate and return the photoemission kernel function
        (see. Dissertation M. Feidt, TU Kaiserslautern (2017) page 55):

        Keyword arguments:
        -----------------------------------------------------------------------
        kz (float):         perpenticular momentum component [nm^-1]
        kx,ky (float):      parallel momentum components [nm^-1]
        w (float):          kinetic energy [eV]
        method (str):       options = ['em_conserved','kz_integrated']
        """
        """ TO DO: include light penetration depth, phase shifts parameter """
        # perpenticular wavevector inside the solid
        ksr = np.real(self.final_state.get_ks(kx,ky,w))
        ksi = np.imag(self.final_state.get_ks(kx,ky,w))

        #add a tiny constant to regularize (avoid division by zero)
        """so far final state has no imaginary part!!!"""
        kl = ksi+1e-10*+ .2#np.sqrt(0.232/self.alpha)
        numerator = kz*kz
        denominator = kl**4 + (ksr**2 - kz**2)**2 + 2.*kl**2*(ksr**2 + kz**2)
        return np.real(numerator/denominator)


    def pes(self,kx,ky,w,method='em_conserved'):
        """
        Calculate and return the photoemission signal

        To do: extend kz_integrated to laser integration

        Keyword arguments:
        -----------------------------------------------------------------------
        kx,ky (float):      parallel momentum components
        w (float):          kinetic energy
        method (str):       options = ['em_conserved','kz_integrated']
        """
        if method == 'em_conserved':

            # check if light source is laser or cw
            if hasattr(self.light_source,'spectral_width'):
                # evalute integral only around the current kinetic energy. other regions should give negligible contributions...
                # check if integral evaluation point (w dependency) is correct?
                lower_bound = w-self.light_source.central_energy - self.light_source.spectral_width*2.
                upper_bound = w-self.light_source.central_energy + self.light_source.spectral_width*2.
                result = quad(self.pes_kernel,lower_bound,upper_bound,args=(kx,ky,w))[0]

            else:
                result = self.pes_kernel(w-self.light_source.central_energy,kx,ky,w)

        elif method == 'kz_integrated':
            """TO DO: extent to laser intergration"""
            kz_mean = np.real(self.final_state.get_ks(kx,ky,w))
            kz_lower_bound = kz_mean - kz_mean*0.2
            kz_upper_bound = kz_mean + kz_mean*0.2

            # check if light source is laser or cw
            if hasattr(self.light_source,'spectral_width'):
                w_lower_bound = w-self.light_source.central_energy - self.light_source.spectral_width*2.
                w_upper_bound = w-self.light_source.central_energy + self.light_source.spectral_width*2.

                kernel = lambda q: (quad(self.pes_kernel_kz_integrated,w_lower_bound,w_upper_bound,args=(kx,ky,q,w))[0])*self.kz_kernel(q,kx,ky,w)
                result = quad(kernel,kz_lower_bound, kz_upper_bound)[0]

            else:
                kernel = lambda q: self.pes_kernel_kz_integrated(w-self.light_source.central_energy,kx,ky,q,w)*self.kz_kernel(q,kx,ky,w)
                result = quad(kernel,kz_lower_bound, kz_upper_bound)[0]
        else:
            #print("unknown method")

            """ test projected surface density of states calculation """
            result = 0.
            for kz in np.linspace(0.,40.,80):
                result += self.spectral_function.value(kx,ky,kz,w-self.light_source.central_energy+self.fermi_energy+self.workfunction)#self.photoemission_kernel2(w-self.light_source.central_energy,kx,ky,kz,w)



        if(np.isnan(result)):
            return -1.
        else:
            return result


    def pes_surface_scat(self,kx,ky,qx,qy,w,method='em_conserved'):
        """
        Calculate and return the photoemission signal with superlattice
        scattering

        To do: implement kz_integrated option

        Keyword arguments:
        -----------------------------------------------------------------------
        kx,ky (float):      parallel momentum components [nm^-1]
        qx,qy (float):      scattering momentum components of the superlattice
                            [nm^-1]
        w (float):          kinetic energy [eV]
        method (str):       options = ['em_conserved','kz_integrated']
        """
        if method == 'em_conserved':

            # check if light source is laser or cw
            if hasattr(self.light_source,'spectral_width'):
                # evalute integral only around the current kinetic energy. other regions should give negligible contributions...
                # check if integral evaluation point (w dependency) is correct?
                lower_bound = w-self.light_source.central_energy - self.light_source.spectral_width*2.
                upper_bound = w-self.light_source.central_energy + self.light_source.spectral_width*2.
                result = quad(self.pes_kernel_surface_scat,lower_bound,upper_bound,args=(kx,ky,qx,qy,w))[0]

            else:
                result = self.pes_kernel_surface_scat(w-self.light_source.central_energy,kx,ky,qx,qy,w)


        #elif method == 'kz_integrated':
        #    """TO DO: extent to laser intergration"""
        #    kz_mean = np.real(self.final_state.get_ks(kx,ky,w))
        #    kz_lower_bound = kz_mean - kz_mean*0.2
        #    kz_upper_bound = kz_mean + kz_mean*0.2

        #    # check if light source is laser or cw
        #    if hasattr(self.light_source,'spectral_width'):
        #        w_lower_bound = w-self.light_source.central_energy - self.light_source.spectral_width*2.
        #        w_upper_bound = w-self.light_source.central_energy + self.light_source.spectral_width*2.

        #kernel = lambda q: (quad(self.pes_kernel_kz_integrated,w_lower_bound,w_upper_bound,args=(kx,ky,q,w,surface))[0])*self.kz_kernel(q,kx,ky,w)
        #        result = quad(kernel,kz_lower_bound, kz_upper_bound)[0]

        #    else:
        #        kernel = lambda q: self.pes_kernel_kz_integrated(w-self.light_source.central_energy,kx,ky,q,w,surface)*self.kz_kernel(q,kx,ky,w)
        #        result = quad(kernel,kz_lower_bound, kz_upper_bound)[0]
        #else:
            #print("unknown method")

        #    result = 0.
        #    for kz in np.linspace(0.,10.,20):
        #        result += self.spectral_function.value(kx,ky,kz,w-self.light_source.central_energy+self.fermi_energy+self.workfunction,surface)#self.photoemission_kernel2(w-self.light_source.central_energy,kx,ky,kz,w)

        if(np.isnan(result)):
            return -1.
        else:
            return result

    def params_str(self):
        pstr = []
        pstr.append("{}={:.2f}{}".format("fermi_energy",self.fermi_energy,os.linesep))
        pstr.append("{}={:.2f}{}".format("workfunction",self.workfunction,os.linesep))
        return "".join(pstr)
