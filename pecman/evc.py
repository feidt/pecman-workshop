# This file is part of PECMAN

# PECMAN is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from abc import ABCMeta, abstractmethod


# eigenvalue calulator class
class EVC(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def eigenenergies(self,kx,ky,kz):
        """ Return a list of eigenvalues of the photohole """
        raise NotImplementedError("Should implement eigenenergies()!")

    @abstractmethod
    def params_str(self):
        """Return a string with all model parameters"""
        raise NotImplementedError("Should implement params_str()")


    @property
    def dim(self):
        return self.__dim

    @dim.setter
    def dim(self, dim):
        self.__dim = dim

    @property
    def model_name(self):
        return self.__model_name

    @model_name.setter
    def model_name(self, model_name):
        self.__model_name = model_name


    @staticmethod
    def format_param(p):
        return "{:5.5f}".format(p)
