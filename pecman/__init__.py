"""Main PECMAN module (the Photoemission Calculation MANager)"""
# If you find problems, please submit bug reports to pecman@runbox.com

# This file is part of PECMAN

# PECMAN is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

__author__ = "Martin Feidt"
__copyright__ = "Copyright 2017 Martin Feidt"
__credits__ = ["Martin Feidt"]
__license__ = "GPLv3"
__version__ = "1.0.0 Alpha"
__maintainer__ = "Martin Feidt"
__email__ = "pecman@runbox.com"
__status__ = "Prototype"

import numpy as np
import sys
import os
from os import getcwd

from datetime import datetime as dti
import matplotlib.pyplot as plt

from pecman.spectralfunction import SpectralFunction
from pecman.intermediatestate import IntermediateState
from pecman.finalstate import ILEEDState
from pecman.evc import EVC
from pecman.lightsource import CWSource, Laser
from pecman.selfenergy import Selfenergy
from pecman.arpes import Arpes
from pecman.tppe import Tppe
from pecman.cisfcc import CombIntSchemeFCC
from pecman.cisparameters import CombIntSchemeFCCParameters
from pecman.models import FreeElectron2DState,\
                          FreeElectron3DState,\
                          TightBindingFCC,\
                          TightBindingCuO2,\
                          TightBindingPeroskite,\
                          TightBindingGraphene,\
                          TightBindingFCCrot

from pecman.pconfig import *
from pecman.phelper import *

# def from pecman import *
__all__ = ['SpectralFunction','IntermediateState','ILEEDState','EVC',
           'CWSource','Laser','Selfenergy','Arpes','Tppe','CombIntSchemeFCC',
           'CombIntSchemeFCCParameters','Pecman',
           'FreeElectron2DState',
           'FreeElectron3DState',
           'TightBindingFCC',
           'TightBindingCuO2',
           'TightBindingPeroskite',
           'TightBindingGraphene',
           'TightBindingFCCrot']


class Pecman(object):

    def __init__(self,experiment,cmap='viridis'):
        """
        Create a Pecman object which manages the PES calculations,
        plotting and saving of data and configurations

        Attributes:
        -----------------------------------------------------------------------
        experiment (Experiment):    valid options are Arpes, Tppe, ...
        cmap (str):                 a Python colormap name

        """
        self.experiment = experiment
        self.cmap = cmap


    @property
    def cmap(self):
        return self._cmap

    @cmap.setter
    def cmap(self, cmap):
        cmap_found = False
        for cm in plt.colormaps():
            if cm == cmap:
                cmap_found = True
        if cmap_found == True:
            self._cmap = cmap
        else:
            print("colormap " + str(cmap) +
                  " is not available! cmap is set to 'jet'")
            self._cmap = 'jet'




    """
    ===========================================================================
                        Momentum Distribution Curves
    ===========================================================================
    """

    def mdc(self,n=20,kmax=21.,be=0.1,rot_angle=0.,method='em_conserved',
            sdir=""):
        """
        Calculate and return a momentum distribution cut (mdc)
        (aka momentum map) as a function of parallel momentum for a particular
        kinetic energy (or binding energy)

        Keyword arguments:
        -----------------------------------------------------------------------
        n (int):           integer resolution
        kmax (float):      maximum k_parallel value in nm^-1
        be (float):        binding energy relative to the fermi level
                           (note: for states below the fermi level
                           be is defined to be a positive number!)
        rot_angle (float): azimuthal (or polar) angle, the effect is to rotate
                           the sample
        method (str):      photoemission kernel method = 'em_conversed',
                           'kz_integrated'
        sdir (str):        name of the save directory, if sdir == "", nothing
                           will be saved
        """

        kxaxis = np.linspace(-kmax,kmax,n)
        kyaxis = np.linspace(-kmax,kmax,n)

        spectrum = np.zeros((n,n),dtype = float)
        N = float(n*n)


        # attention: for 2PPE the photon energy has to be adjusted, factor 2!!!! """
        if self.experiment.exptype == "tppe":
            # kinetic energy
            w = self.experiment.light_source.central_energy*2.-self.experiment.workfunction-be
        else:
            # kinetic energy
            w = self.experiment.light_source.central_energy-self.experiment.workfunction-be

        # convert from degree to rad
        delta = rot_angle*np.pi/180.

        progress = 0.
        start = dti.now()

        for iky,ky in enumerate(kyaxis):
            for ikx,kx in enumerate(kxaxis):
                kxn,kyn = self.polar_rot(kx,ky,delta)
                spectrum[iky,ikx] = self.experiment.pes(kxn,kyn,w,method)

                progress = self.update_progress((iky*ikx)/N,progress)

        end = dti.now()
        print('\r' + self.time_stamp_str() + "  " + "100.00% completed")

        total_time = str(end-start)
        print("total time: " + total_time)

        # save data
        function_tag =  "mdc"
        data_type = ".txt"
        filename = self.generate_filename(sdir, function_tag, data_type)

        if filename != "":
            # save function parameters and spectrum
            header = []
            header.append("{}={:d}{}".format("n",n,os.linesep))
            header.append("{}={:.2f}{}".format("kmax",kmax,os.linesep))
            header.append("{}={:.2f}{}".format("be",be,os.linesep))
            header.append("{}={:.2f}{}".format("rot_angle",rot_angle,os.linesep))
            header.append("{}={}{}".format("method",method,os.linesep))
            header.append("{}={}".format("total time",total_time))
            np.savetxt(filename, spectrum, fmt='%1.5e', header = "".join(header))

            # save model parameters
            model_name = self.experiment.spectral_function.evc.model_name
            params = self.experiment.spectral_function.evc.params_str()
            self.save_params_str(sdir,model_name,params)


        return spectrum


    def mdc_surf_scat_test(self,n=20,kmax=21.,be=0.1,rot_angle=0.,
                           sdir="",method='em_conserved'):

        kxaxis = np.linspace(-kmax,kmax,n)
        kyaxis = np.linspace(-kmax,kmax,n)

        spectrum = np.zeros((n,n),dtype = float)
        N = float(n*n)

        # kinetic energy
        w = self.experiment.light_source.central_energy-self.experiment.workfunction-be


        l1=0.5
        l2=0.5
        L=2.5
        ang=60.*np.pi/180.

        bx = 2.*np.sqrt(3.)*L*np.cos(ang)
        by = 2.*np.sqrt(3.)*L*np.sin(ang)


        # convert from degree to rad
        delta = rot_angle*np.pi/180.
        #bxn, byn = self.polar_rot(bx,by,delta)

        progress = 0.
        start = dti.now()
        for iky,ky in enumerate(kyaxis):
            for ikx,kx in enumerate(kxaxis):
                kxn,kyn = self.polar_rot(kx,ky,delta)
                #a1,b1 = self.polar_rot(0.,2.*by,delta)
                spectrum[ikx,iky] = self.experiment.pes_surface_scat(kxn,kyn,0.,0.,w,method)+\
                                    self.experiment.pes_surface_scat(kxn,kyn,0.,2.*by,w,method)+\
                                    self.experiment.pes_surface_scat(kxn,kyn,bx,3.*by,w,method)+self.experiment.pes_surface_scat(kxn,kyn,-bx,3.*by,w,method)+\
                                    self.experiment.pes_surface_scat(kxn,kyn,-4.*bx,-2.*by,w,method)#+self.experiment.pes_surface_scat(kxn,kyn,-5.*bx,-1.*by,w,method,surface)+\
                                    #self.experiment.pes_surface_scat(kxn,kyn,-3.*bx,-1.*by,w,method,surface)+\
                                    #self.experiment.pes_surface_scat(kxn,kyn,3.*bx,-1.*by,w,method,surface)+self.experiment.pes_surface_scat(kxn,kyn,4.*bx,-2.*by,w,method,surface)+\
                                    #self.experiment.pes_surface_scat(kxn,kyn,5.*bx,-1.*by,w,method,surface)

                progress = self.update_progress((iky*ikx)/N,progress)

        end = dti.now()
        print(self.time_stamp_str() + "  " + "100.00% completed")
        total_time = str(end-start)
        print("total time: " + total_time)

        # save data
        function_tag =  "mdc"
        data_type = ".txt"
        filename = self.generate_filename(sdir, function_tag, data_type)
        if filename != "":
            # save function parameters and spectrum
            header = []
            header.append("{}={:d}{}".format("n",n,os.linesep))
            header.append("{}={:.2f}{}".format("kmax",kmax,os.linesep))
            header.append("{}={:.2f}{}".format("be",be,os.linesep))
            header.append("{}={:.2f}{}".format("rot_angle",rot_angle,os.linesep))
            header.append("{}={}{}".format("method",method,os.linesep))
            header.append("{}={}".format("total time",total_time))
            np.savetxt(filename, spectrum, fmt='%1.5e', header = "".join(header))

            # save model parameters
            model_name = self.experiment.spectral_function.evc.model_name
            params = self.experiment.spectral_function.evc.params_str()
            self.save_params_str(sdir,model_name,params)


        return spectrum




    def mdc_surf_scat(self,n=20,kmax=21.,be=0.1,rot_angle=0.,sdir="",
                      method='em_conserved',scat_list=None):
        """
        Calculate and return a momentum distribution cut (mdc)
        (aka momentum map) as a function of parallel momentum for a particular
        kinetic energy (or binding energy) with additional superlattice
        scattering

        Keyword arguments:
        -----------------------------------------------------------------------
        n (int):                integer resolution
        kmax (float):           maximum k_parallel value in nm^-1
        be (float):             binding energy relative to the fermi level
                                (note: for states below the fermi level
                                be is defined to be a positive number!)
        rot_angle (float):      azimuthal (or polar) angle, the effect is to
                                rotate the sample
        sdir (str):             name of the save directory, if sdir == "",
                                nothing will be saved
        method (str):           photoemission kernel method = 'em_conversed',
                                'kz_integrated'
        scat_list (float[]):    array of 2D momentum vector pairs (as floats)
                                e.g. [[0,0.2],[0.4,0.6],...]
                                entries are multiples of an inverse 2d lattice
                                vector
        """
        kxaxis = np.linspace(-kmax,kmax,n)
        kyaxis = np.linspace(-kmax,kmax,n)

        spectrum = np.zeros((n,n),dtype = float)
        # use test right after creating spectrum, to return at least an empty matrix
        if isinstance(scat_list, np.ndarray):
            if scat_list.shape[1] == 2:
                N = float(n*n)
                # kinetic energy
                w = self.experiment.light_source.central_energy-self.experiment.workfunction-be

                # convert from degree to rad
                delta = rot_angle*np.pi/180.

                progress = 0.
                start = dti.now()
                for iky,ky in enumerate(kyaxis):
                    for ikx,kx in enumerate(kxaxis):
                        kxn,kyn = self.polar_rot(kx,ky,delta)
                        signal = 0.
                        for vector in scat_list:
                            signal += self.experiment.pes_surface_scat(kxn,kyn,vector[0],vector[1],w,method)
                        #spectrum[ikx,iky] = sum([self.experiment.pes_surface_scat(kxn,kyn,vector[0],vector[1],w,method,surface) for vector in scat_list])
                        spectrum[ikx,iky] = signal

                        progress = self.update_progress((iky*ikx)/N,progress)

                end = dti.now()
                print(self.time_stamp_str() + "  " + "100.00% completed")
                total_time = str(end-start)
                print("total time: " + total_time)

                # save data
                function_tag =  "mdc"
                data_type = ".txt"
                filename = self.generate_filename(sdir, function_tag, data_type)
                if filename != "":
                    # save function parameters and spectrum
                    header = []
                    header.append("{}={:d}{}".format("n",n,os.linesep))
                    header.append("{}={:.2f}{}".format("kmax",kmax,os.linesep))
                    header.append("{}={:.2f}{}".format("be",be,os.linesep))
                    header.append("{}={:.2f}{}".format("rot_angle",rot_angle,os.linesep))
                    header.append("{}={}{}".format("method",method,os.linesep))
                    header.append("{}={}".format("total time",total_time))
                    np.savetxt(filename, spectrum, fmt='%1.5e', header = "".join(header))

                    # save model parameters
                    model_name = self.experiment.spectral_function.evc.model_name
                    params = self.experiment.spectral_function.evc.params_str()
                    self.save_params_str(sdir,model_name,params)
            else:
                print("super-lattice vector dimension != 2")

        else:
            print("no superlattice vectors given...")
        return spectrum




    """
    ===========================================================================
                        Energy Distribution Curves
    ===========================================================================
    """


    def edc(self,n=50,kmax=21.,be_min=7.,be_max=0.,rot_angle=90.,
            method='em_conserved',sdir=""):
        """
        Calculate and return an energy distribution cut (edc)
        (conventional ARPES spectrum) as a function of parallel momentum

        Keyword arguments:
        -----------------------------------------------------------------------
        n (int):            integer resolution
        kmax (float):       maximum k_parallel value in nm^-1
        be_min (float):     minimum binding energy below the fermi level
                            (note: defined to be a positive number!)
        be_max (float):     maximum binding energy above the fermi level
                            (also a positive number)
        rot_angle (float):  azimuthal (or polar) angle for the cut
        method (str):       photoemission kernel method = 'em_conversed',
                            'kz_integrated'
        sdir (str):         name of the save directory, if sdir == "",
                            nothing will be saved
        """

        # attention: for 2PPE the photon energy has to be adjusted, factor 2!!!! """
        if self.experiment.exptype == "tppe":
            Ekin_max = self.experiment.light_source.central_energy*2. - self.experiment.workfunction
        else:
            Ekin_max = self.experiment.light_source.central_energy - self.experiment.workfunction


        Eaxis = np.linspace(Ekin_max-be_min,Ekin_max+be_max,n)
        kaxis = np.linspace(-kmax,kmax,n)

        spectrum = np.zeros((n,n),dtype = float)
        N = float(n*n)

        # convert from degree to rad
        delta = rot_angle*np.pi/180.

        progress = 0.
        start = dti.now()

        # calulcate spectrum
        for iw,w in enumerate(Eaxis[::-1]):
            for ik,k in enumerate(kaxis):
                kxn,kyn = self.polar_rot(k,0.,delta)
                spectrum[iw,ik] = self.experiment.pes(kxn,kyn,w,method)

                # print progress on terminal
                progress = self.update_progress((iw*ik)/N,progress)

        end = dti.now()
        print('\r' + self.time_stamp_str() + "  " + "100.00% completed")
        total_time = str(end-start)
        print("total time: " + total_time)

        # save data
        function_tag =  "edc"
        data_type = ".txt"
        filename = self.generate_filename(sdir, function_tag, data_type)
        if filename != "":
            # save function parameters and spectrum
            header = []
            header.append("{}={:d}{}".format("n",n,os.linesep))
            header.append("{}={:.2f}{}".format("kmax",kmax,os.linesep))
            header.append("{}={:.2f}{}".format("be_min",be_min,os.linesep))
            header.append("{}={:.2f}{}".format("be_max",be_max,os.linesep))
            header.append("{}={:.2f}{}".format("rot_angle",rot_angle,os.linesep))
            header.append("{}={}{}".format("method",method,os.linesep))
            header.append("{}={}".format("total time",total_time))
            np.savetxt(filename, spectrum, fmt='%1.5e', header = "".join(header))

            # save model parameters
            model_name = self.experiment.spectral_function.evc.model_name
            params = self.experiment.spectral_function.evc.params_str()
            self.save_params_str(sdir,model_name,params)

        return spectrum






    def edc_photon_seq(self,n=50,kmax=21.,be_min=7.,be_max=0.,rot_angle=90.,
                       method='em_conserved',m=1,sigma=1.,sdir=""):
        """
        Calculate and save an energy distribution cut (edc) sequence for
        different photon energies in the range
        [photon_energy-sigma/2,photon_energy+sigma/2]

        To do: return array of edcs

        Keyword arguments:
        -----------------------------------------------------------------------
        n (int):            integer resolution
        kmax (float):       maximum k_parallel value in nm^-1
        be_min (float):     minimum binding energy below the fermi level
                            (note: defined to be a positive number!)
        be_max (float):     maximum binding energy above the fermi level
                            (also a positive number)
        rot_angle (float):  azimuthal (or polar) angle for the cut
        method (str):       photoemission kernel method = 'em_conversed',
                            'kz_integrated'
        m (int):            number of photon energy values in the range
                            [central_energy-sigma/2,central_energy+sigma/2]
        sigma (float):      photon energy range in eV
        sdir (str):         name of the save directory, if sdir == "",
                            nothing will be saved
        """

        kaxis = np.linspace(-kmax,kmax,n)
        if sigma < 0:
            sigma = -sigma

        # store original central energy, because this has to be changed for
        # the sequence and has to be restored at the end
        central_energy = self.experiment.light_source.central_energy
        pe_min = central_energy-sigma*0.5
        pe_max = central_energy+sigma*0.5
        Paxis = np.linspace(pe_min,pe_max,m)

        N = float(n*n)

        # convert from degree to rad
        delta = rot_angle*np.pi/180.

        # save model parameters
        model_name = self.experiment.spectral_function.evc.model_name
        params = self.experiment.spectral_function.evc.params_str()
        self.save_params_str(sdir,model_name,params)

        # calculate photon sequence
        for ipe, pe in enumerate(Paxis):

            # change the photon energy to modify also the perp. k values
            self.experiment.light_source.central_energy = pe

            progress = 0.
            start = dti.now()

            spectrum = np.zeros((n,n),dtype = float)

            # generate energy values based on current photon energy (pe)
            Ekin_max = pe - self.experiment.workfunction
            Eaxis = np.linspace(Ekin_max-be_min,Ekin_max+be_max,n)

            # calulcate spectrum
            for iw,w in enumerate(Eaxis[::-1]):
                for ik,k in enumerate(kaxis):
                    kxn,kyn = self.polar_rot(k,0.,delta)
                    spectrum[iw,ik] = self.experiment.pes(kxn,kyn,w,method)

                    # print progress on terminal
                    progress = self.update_progress((iw*ik)/N,progress)

            end = dti.now()
            print('\r' + self.time_stamp_str() + "  " + "100.00% completed")
            total_time = str(end-start)
            print("total time: " + total_time)

            # save data
            function_tag =  str(ipe)
            data_type = ".txt"
            filename = self.generate_filename(sdir, function_tag, data_type)
            if filename != "":
                # save function parameters and spectrum
                header = []
                header.append("{}={:d}{}".format("n",n,os.linesep))
                header.append("{}={:.2f}{}".format("kmax",kmax,os.linesep))
                header.append("{}={:.2f}{}".format("be_min",be_min,os.linesep))
                header.append("{}={:.2f}{}".format("be_max",be_max,os.linesep))
                header.append("{}={:.2f}{}".format("rot_angle",rot_angle,os.linesep))
                header.append("{}={}{}".format("method",method,os.linesep))
                header.append("{}={:.2f}{}".format("photon_energy",pe,os.linesep))
                header.append("{}={:.2f}{}".format("sigma",sigma,os.linesep))
                header.append("{}={}".format("total time",total_time))
                np.savetxt(filename, spectrum, fmt='%1.5e', header = "".join(header))

        # restore the original photon energy
        self.experiment.light_source.central_energy = central_energy



    """EXPERIMENTAL"""
    def ped(self,n=20,kmax=21.,kx=0.,ky=0.,be=0.1,rot_angle=0.,
            method='em_conserved',sdir=""):
        """

        Keyword arguments:
        -----------------------------------------------------------------------
        n (int):            integer resolution
        kmax (float):       maximum k_parallel value in nm^-1
        be (float):         binding energy relative to the fermi level
                            (note: for states below the fermi level
                            be is defined to be a positive number!)
        rot_angle (float):  azimuthal (or polar) angle, the effect is to rotate
                            the sample
        method (str):      photoemission kernel method = 'em_conversed',
                           'kz_integrated'
        sdir (str):         name of the save directory, if sdir == "", nothing
                            will be saved
        """

        thetaaxis = np.linspace(0.,np.pi,n)
        phiaxis = np.linspace(0.,2.*np.pi,n)

        spectrum = np.zeros((n,n),dtype = float)
        N = float(n*n)

        # kinetic energy
        w = self.experiment.light_source.central_energy-self.experiment.workfunction-be

        # convert from degree to rad
        delta = rot_angle*np.pi/180.

        progress = 0.
        start = dti.now()

        for itheta,theta in enumerate(thetaaxis):
            for iphi,phi in enumerate(phiaxis):
                kxn,kyn = self.polar_rot(kx,ky,delta)
                spectrum[itheta,iphi] = self.experiment.pes_ped(kxn,kyn,w,method,theta,phi)

                progress = self.update_progress((itheta*iphi)/N,progress)

        end = dti.now()
        print('\r' + self.time_stamp_str() + "  " + "100.00% completed")

        total_time = str(end-start)
        print("total time: " + total_time)

        # save data
        function_tag =  "mdc"
        data_type = ".txt"
        filename = self.generate_filename(sdir, function_tag, data_type)

        if filename != "":
            # save function parameters and spectrum
            header = []
            header.append("{}={:d}{}".format("n",n,os.linesep))
            header.append("{}={:.2f}{}".format("kmax",kmax,os.linesep))
            header.append("{}={:.2f}{}".format("be",be,os.linesep))
            header.append("{}={:.2f}{}".format("rot_angle",rot_angle,os.linesep))
            header.append("{}={}{}".format("method",method,os.linesep))
            header.append("{}={}".format("total time",total_time))
            np.savetxt(filename, spectrum, fmt='%1.5e', header = "".join(header))

            # save model parameters

            model_name = self.experiment.spectral_function.evc.model_name
            params = self.experiment.spectral_function.evc.params_str()
            self.save_params_str(sdir,model_name,params)


        return spectrum



    """
    ===========================================================================
                        Constant (Binding) Energy Curves
    ===========================================================================
    """


    def cec(self,n=50,kmax=21.,ky=0.,be=0.1,rot_angle=90.,
            method='em_conserved',sigma=1.,sdir=""):
        """
        Calculate and return a constant (binding) energy curve, i.e.
        photon energy vs. parallel momentum for a fixed binding energy

        Keyword arguments:
        ------------------------------------------------------------------------
        n (int):            integer resolution
        kmax (float):       maximum k_parallel value in nm^-1
        ky (float):         ky momentum in nm^-1
        be (float):         binding energy below the fermi level (note: defined
                            to be a positive number!)
        rot_angle (float):  azimuthal (or polar) angle for the cut
        method (str):       photoemission kernel method = 'em_conversed',
                            'kz_integrated'
        sigma (float):      photon energy interval in eV
        sdir (str):         name of the save directory, if sdir == "", nothing
                            will be saved
        """
        #Ekin_max = self.experiment.light_source.central_energy - self.experiment.workfunction
        #Eaxis = np.linspace(Ekin_max-be_min,Ekin_max+be_max,n)
        kaxis = np.linspace(-kmax,kmax,n)

        if sigma < 0:
            sigma = -sigma

        # store original central energy, because this has to be changed for the sequence and has to be restored at the end
        central_energy = self.experiment.light_source.central_energy
        pe_min = central_energy-sigma*0.5
        pe_max = central_energy+sigma*0.5
        Paxis = np.linspace(pe_min,pe_max,n)

        spectrum = np.zeros((n,n),dtype = float)
        N = float(n*n)

        # convert from degree to rad
        delta = rot_angle*np.pi/180.

        progress = 0.
        start = dti.now()


        # calulcate spectrum
        for iw,w in enumerate(Paxis[::-1]):

            # change the photon energy to modify also the k_perpendicular values
            self.experiment.light_source.central_energy = w
            binding_energy = w - self.experiment.workfunction - be
            for ik,k in enumerate(kaxis):
                kxn,kyn = self.polar_rot(k,ky,delta)
                spectrum[iw,ik] = self.experiment.pes(kxn,kyn,binding_energy,method)

                # print progress on terminal
                progress = self.update_progress((iw*ik)/N,progress)

        end = dti.now()
        print('\r' + self.time_stamp_str() + "  " + "100.00% completed")
        total_time = str(end-start)
        print("total time: " + total_time)

        # save data
        function_tag =  "cec"
        data_type = ".txt"
        filename = self.generate_filename(sdir, function_tag, data_type)
        if filename != "":
            # save function parameters and spectrum
            header = []
            header.append("{}={:d}{}".format("n",n,os.linesep))
            header.append("{}={:.2f}{}".format("kmax",kmax,os.linesep))
            header.append("{}={:.2f}{}".format("be",be,os.linesep))
            header.append("{}={:.2f}{}".format("rot_angle",rot_angle,os.linesep))
            header.append("{}={}{}".format("method",method,os.linesep))
            header.append("{}={:.2f}{}".format("sigma",sigma,os.linesep))
            header.append("{}={}".format("total time",total_time))
            np.savetxt(filename, spectrum, fmt='%1.5e', header = "".join(header))

            # save model parameters
            model_name = self.experiment.spectral_function.evc.model_name
            params = self.experiment.spectral_function.evc.params_str()
            self.save_params_str(sdir,model_name,params)

        # restore the original photon energy
        self.experiment.light_source.central_energy = central_energy

        return spectrum





    """
    ===========================================================================
                        Constant Momentum Curves
    ===========================================================================
    """

    """EXPERIMENTAL"""
    #########
    #########   change from kaxis to Eaxis !!!!!!!!!!!!!!!!!!!!!!!!!!
    #########
    def cmc(self,n=50,kx=0.,ky=0.,be=0.1,rot_angle=0.,method='em_conserved',
            sigma=1.,sdir=""):
        """
        Calculate and return a constant (binding) energy curve, i.e. photon
        energy vs. parallel momentum for a fixed binding energy

        Keyword arguments:
        -----------------------------------------------------------------------
        n (int):            integer resolution
        kmax (float):       maximum k_parallel value in nm^-1
        kx (float):         kx momentum in nm^-1
        ky (float):         ky momentum in nm^-1
        be (float):     binding energy in eV
        rot_angle (float):  azimuthal (or polar) angle for the cut
        method (str):       photoemission kernel method = 'em_conversed',
                            'kz_integrated'
        sigma (float):      photon energy interval in eV
        sdir (str):         name of the save directory, if sdir == "",
                            nothing will be saved
        """


        """
        def Ek_normal_emission_omega_series(n,Omin,Omax,m,Ef,Emin,Emax,surface,delta):


            envals = np.linspace(-Emin,Emax,n)
            Oaxis = np.linspace(Omax,Omin,m)
            #kaxis = np.linspace(-maxk,maxk,n)
            data = np.zeros((m,n),dtype = complex)
            N = float(n*m)
            percentage = 0.
            for idO,Omega in enumerate(Oaxis):
                maxEkin = Omega-Wf
                Eaxis = np.linspace(maxEkin-Emin,maxEkin+Emax,n)
                for iw,w in enumerate(Eaxis):
                    data[idO,iw] = pes2dHMFall(w,0.,0,Omega,Ef,surface)
        """
        #Ekin_max = self.experiment.light_source.central_energy - self.experiment.workfunction
        #Eaxis = np.linspace(Ekin_max-be_min,Ekin_max+be_max,n)
        kaxis = np.linspace(-kmax,kmax,n)

        if sigma < 0:
            sigma = -sigma

        # store original central energy, because this has to be changed for the sequence and has to be restored at the end
        central_energy = self.experiment.light_source.central_energy
        pe_min = central_energy-sigma*0.5
        pe_max = central_energy+sigma*0.5
        Paxis = np.linspace(pe_min,pe_max,m)

        spectrum = np.zeros((n,n),dtype = float)
        N = float(n*n)

        #convert from degree to rad
        delta = rot_angle*np.pi/180.

        progress = 0.
        start = dti.now()


        #calulcate spectrum
        for iw,w in enumerate(Paxis[::-1]):

            #change the photon energy to modify also the k_perpendicular values
            self.experiment.light_source.central_energy = w
            binding_energy = w - self.experiment.workfunction - be
            for ik,k in enumerate(kaxis):
                kxn,kyn = self.polar_rot(k,ky,delta)
                spectrum[iw,ik] = self.experiment.pes(kxn,kyn,binding_energy,method)

                #print progress on terminal
                progress = self.update_progress((iw*ik)/N,progress)

        end = dti.now()
        print('\r' + self.time_stamp_str() + "  " + "100.00% completed")
        total_time = str(end-start)
        print("total time: " + total_time)

        #save data
        function_tag =  "cmc"
        data_type = ".txt"
        filename = self.generate_filename(sdir, function_tag, data_type)
        if filename != "":
            # save function parameters and spectrum
            header = []
            header.append("{}={:d}{}".format("n",n,os.linesep))
            header.append("{}={:.2f}{}".format("kmax",kmax,os.linesep))
            header.append("{}={:.2f}{}".format("be",be,os.linesep))
            header.append("{}={:.2f}{}".format("rot_angle",rot_angle,os.linesep))
            header.append("{}={}{}".format("method",method,os.linesep))
            header.append("{}={}".format("total time",total_time))
            np.savetxt(filename, spectrum, fmt='%1.5e', header = "".join(header))

            #save model parameters
            model_name = self.experiment.spectral_function.evc.model_name
            params = self.experiment.spectral_function.evc.params_str()
            self.save_params_str(sdir,model_name,params)

        #restore the original photon energy
        self.experiment.light_source.central_energy = central_energy

        return spectrum



    """
    ===========================================================================
                        INITIAL/INTERMEDIATE STATE SPECTRUM
    ===========================================================================
    """


    def spectrum_fcc(self,n=100,dk=0.4,be_min=7,be_max=1,lc=0.41,
                     mode='all',sdir=''):
        """
        Calculate the spectrum of initial/intermediate states along high
        symmetry points of an FCC crystal: G-X-W-L-G-K,
        as in Laesser, Smith PRB 24 (1981)

        Keyword arguments:
        -----------------------------------------------------------------------
        n (int):          integer energy resolution
        dk (float):       momentum resolution [nm^-1],
                          e.g. (2 pi/a) / 100 ~ 0.15
        be_min (float):   minimum binding energy below the fermi level
                          (note: defined to be a positive number!)
        be_max (float):   maximum binding energy above the fermi level
                          (also a positive number)
        lc (float):       lattice constant in nm
        mode (str):       valid options: 'all','initial','intermediate'
        sdir (str):       name of the save directory, if sdir == "",
                          nothing will be saved
        """

        """ TO DO: check if intermediate option is valid should be made here -> remove redundant checks ..."""
        #if self.experiment.spectral_function.evc.__class__.__name__ == "CombIntSchemeFCC":

        # note: for the simple tight binding models, the lattice parameter is directly implemented in EVC, for the CIS it`s in the parameters class
        # lattice_param = self.evc.params.a
        lattice_param = lc
        bzp = np.pi/lattice_param

        l_gx = 2.*bzp
        l_xw = bzp
        l_wl = np.sqrt(2.)*bzp
        l_lg = np.sqrt(3.)*bzp
        l_gk = 3.*np.sqrt(1./2.)*bzp

        n_gx = int(l_gx/dk)
        n_xw = int(l_xw/dk)
        n_wl = int(l_wl/dk)
        n_lg = int(l_lg/dk)
        n_gk = int(l_gk/dk)

        band_dim = self.experiment.spectral_function.evc.dim
        k_dim = n_gx + n_xw + n_wl + n_lg + n_gk

        spectrum = np.zeros((n, k_dim),dtype=float)

        N = float(n)
        progress = 0.
        start = dti.now()

        # ugly hack, bandstructe is calculated along high symmetry point referenced by '001' direction
        backup_surface = self.experiment.spectral_function.surface
        self.experiment.spectral_function.surface = ''

        # walk through energy values
        for iw,w in enumerate(np.linspace(-be_min,be_max,n)):

            if(iw/N >= progress):
                #print progress on terminal
                progress = self.update_progress((iw)/N,progress)

            # G-X
            for ik, k in enumerate(np.linspace(0., 2.*bzp,n_gx)):
                if mode == 'initial':
                    spectrum[iw,ik] = self.experiment.spectral_function.value(tiny, tiny+k, tiny,w)
                elif mode == 'intermediate':
                    if isinstance(self.experiment,Tppe):
                        spectrum[iw,ik] = np.abs(np.imag(self.experiment.intermediate_state.value(tiny, tiny+k, tiny,w)))
                    else:
                        print("experiment has no intermediate state!")
                        return
                elif mode == 'all':
                    if isinstance(self.experiment,Tppe):
                        spectrum[iw,ik] = self.experiment.spectral_function.value(tiny, tiny+k, tiny,w) +np.abs(np.imag(self.experiment.intermediate_state.value(tiny, tiny+k, tiny,w)))
                    else:
                        spectrum[iw,ik] = self.experiment.spectral_function.value(tiny, tiny+k, tiny,w)
                else:
                    spectrum[iw,ik] = self.experiment.spectral_function.value(tiny, tiny+k, tiny,w)

            # X-W
            for ik, k in enumerate(np.linspace(0., bzp,n_xw)):
                if mode == 'initial':
                    spectrum[iw,ik+n_gx] = self.experiment.spectral_function.value(tiny+k, 2.*bzp, tiny,w)
                elif mode == 'intermediate':
                    if isinstance(self.experiment,Tppe):
                        spectrum[iw,ik+n_gx] = np.abs(np.imag(self.experiment.intermediate_state.value(tiny+k, 2.*bzp, tiny,w)))
                    else:
                        print("experiment has no intermediate state!")
                        return
                elif mode == 'all':
                    if isinstance(self.experiment,Tppe):
                        spectrum[iw,ik+n_gx] = self.experiment.spectral_function.value(tiny+k, 2.*bzp, tiny,w) +np.abs(np.imag(self.experiment.intermediate_state.value(tiny+k, 2.*bzp, tiny,w)))
                    else:
                        spectrum[iw,ik+n_gx] = self.experiment.spectral_function.value(tiny+k, 2.*bzp, tiny,w)
                else:
                    spectrum[iw,ik+n_gx] = self.experiment.spectral_function.value(tiny+k, 2.*bzp, tiny,w)

            #W-L
            for ik, k in enumerate(np.linspace(0., bzp, n_wl)):
                if mode == 'initial':
                    spectrum[iw,ik+n_gx+n_xw] = self.experiment.spectral_function.value(bzp, 2.*bzp-k, tiny+k,w)
                elif mode == 'intermediate':
                    if isinstance(self.experiment,Tppe):
                        spectrum[iw,ik+n_gx+n_xw] = np.abs(np.imag(self.experiment.intermediate_state.value(bzp, 2.*bzp-k, tiny+k,w)))
                    else:
                        print("experiment has no intermediate state!")
                        return
                elif mode == 'all':
                    if isinstance(self.experiment,Tppe):
                        spectrum[iw,ik+n_gx+n_xw] = self.experiment.spectral_function.value(bzp, 2.*bzp-k, tiny+k,w) +np.abs(np.imag(self.experiment.intermediate_state.value(bzp, 2.*bzp-k, tiny+k,w)))
                    else:
                        spectrum[iw,ik+n_gx+n_xw] = self.experiment.spectral_function.value(bzp, 2.*bzp-k, tiny+k,w)
                else:
                    spectrum[iw,ik+n_gx+n_xw] = self.experiment.spectral_function.value(bzp, 2.*bzp-k, tiny+k,w)

            # L-G
            for ik, k in enumerate(np.linspace(0., bzp, n_lg)):
                #spectrum[iw,ik+n_gx+n_xw+n_wl] = self.experiment.spectral_function.value(bzp-k + tiny, bzp-k + tiny, bzp-k+tiny,w)
                if mode == 'initial':
                    spectrum[iw,ik+n_gx+n_xw+n_wl] = self.experiment.spectral_function.value(bzp-k + tiny, bzp-k + tiny, bzp-k+tiny,w)
                elif mode == 'intermediate':
                    if isinstance(self.experiment,Tppe):
                        spectrum[iw,ik+n_gx+n_xw+n_wl] = np.abs(np.imag(self.experiment.intermediate_state.value(bzp-k + tiny, bzp-k + tiny, bzp-k+tiny,w)))
                    else:
                        print("experiment has no intermediate state!")
                        return
                elif mode == 'all':
                    if isinstance(self.experiment,Tppe):
                        spectrum[iw,ik+n_gx+n_xw+n_wl] = self.experiment.spectral_function.value(bzp-k + tiny, bzp-k + tiny, bzp-k+tiny,w) +np.abs(np.imag(self.experiment.intermediate_state.value(bzp-k + tiny, bzp-k + tiny, bzp-k+tiny,w)))
                    else:
                        spectrum[iw,ik+n_gx+n_xw+n_wl] = self.experiment.spectral_function.value(bzp-k + tiny, bzp-k + tiny, bzp-k+tiny,w)
                else:
                    spectrum[iw,ik+n_gx+n_xw+n_wl] = self.experiment.spectral_function.value(bzp-k + tiny, bzp-k + tiny, bzp-k+tiny,w)

            # G - K
            for ik, k in enumerate(np.linspace(0., 1.5*bzp, n_gk)):
                if mode == 'initial':
                    spectrum[iw,ik+n_gx+n_xw+n_wl+n_lg] = self.experiment.spectral_function.value(tiny+k, tiny+k, tiny,w)
                elif mode == 'intermediate':
                    if isinstance(self.experiment,Tppe):
                        spectrum[iw,ik+n_gx+n_xw+n_wl+n_lg] = np.abs(np.imag(self.experiment.intermediate_state.value(tiny+k, tiny+k, tiny,w)))
                    else:
                        print("experiment has no intermediate state!")
                        return
                elif mode == 'all':
                    if isinstance(self.experiment,Tppe):
                        spectrum[iw,ik+n_gx+n_xw+n_wl+n_lg] = self.experiment.spectral_function.value(tiny+k, tiny+k, tiny,w) +np.abs(np.imag(self.experiment.intermediate_state.value(tiny+k, tiny+k, tiny,w)))
                    else:
                        spectrum[iw,ik+n_gx+n_xw+n_wl+n_lg] = self.experiment.spectral_function.value(tiny+k, tiny+k, tiny,w)
                else:
                    spectrum[iw,ik+n_gx+n_xw+n_wl+n_lg] = self.experiment.spectral_function.value(tiny+k, tiny+k, tiny,w)


        # restore the surface, ulgy solution, has to be updated!!!
        self.experiment.spectral_function.surface = backup_surface

        end = dti.now()
        print('\r' + self.time_stamp_str() + "  " + "100.00% completed")
        total_time = str(end-start)
        print("total time: " + total_time)

        # save data
        function_tag =  "spectrum_fcc"
        data_type = ".txt"
        filename = self.generate_filename(sdir, function_tag, data_type)
        if filename != "":
            # save function parameters and spectrum
            header = []
            header.append("{}={:d}{}".format("n",n,os.linesep))
            header.append("{}={:.2f}{}".format("dk",dk,os.linesep))
            header.append("{}={:.2f}{}".format("be_min",be_min,os.linesep))
            header.append("{}={:.2f}{}".format("be_max",be_max,os.linesep))
            header.append("{}={:.2f}{}".format("lc",lc,os.linesep))
            header.append("{}={}".format("total time",total_time))
            np.savetxt(filename, spectrum, fmt='%1.5e', header = "".join(header))

        return spectrum




    def spectrum_fcc_lgk(self,n=100,dk=0.4,be_min=7,be_max=1,lc=0.41,
                         mode='all',sdir=''):
        """
        Calculate the spectrum of initial/intermediate states
        along high symmetry points of an FCC crystal: L-G-K,

        Keyword arguments:
        -----------------------------------------------------------------------
        n (int):          integer energy resolution
        dk (float):       momentum resolution [nm^-1],
                          e.g. (2 pi/a) / 100 ~ 0.15
        be_min (float):   minimum binding energy below the fermi level
                          (note: defined to be a positive number!)
        be_max: (float)   maximum binding energy above the fermi level
                          (also a positive number)
        lc (float):       lattice constant in nm
        mode (str):       valid options: 'all','initial','intermediate'
        sdir (str):       name of the save directory, if sdir == "", nothing
                          will be saved
        """

        """ TO DO: check if intermediate option is valid should be made here -> remove redundant checks ..."""
        #if self.experiment.spectral_function.evc.__class__.__name__ == "CombIntSchemeFCC":

        #note: for the simple tight binding models, the lattice parameter is directly implemented in EVC, for the CIS it`s in the parameters class
        #lattice_param = self.evc.params.a
        lattice_param = lc
        bzp = np.pi/lattice_param

        l_lg = np.sqrt(3.)*bzp
        l_gk = 3.*np.sqrt(1./2.)*bzp

        n_lg = int(l_lg/dk)
        n_gk = int(l_gk/dk)

        band_dim = self.experiment.spectral_function.evc.dim
        k_dim = n_lg + n_gk

        spectrum = np.zeros((n, k_dim),dtype=float)

        N = float(n)
        progress = 0.
        start = dti.now()

        # ugly hack, bandstructe is calculated along high symmetry point referenced by '001' direction
        backup_surface = self.experiment.spectral_function.surface
        self.experiment.spectral_function.surface = ''

        # walk through energy values
        for iw,w in enumerate(np.linspace(-be_min,be_max,n)):

            if(iw/N >= progress):
                #print progress on terminal
                progress = self.update_progress((iw)/N,progress)


            # L-G
            for ik, k in enumerate(np.linspace(0., bzp, n_lg)):
                #spectrum[iw,ik+n_gx+n_xw+n_wl] = self.experiment.spectral_function.value(bzp-k + tiny, bzp-k + tiny, bzp-k+tiny,w)
                if mode == 'initial':
                    spectrum[iw,ik] = self.experiment.spectral_function.value(bzp-k + tiny, bzp-k + tiny, bzp-k+tiny,w)
                elif mode == 'intermediate':
                    if isinstance(self.experiment,Tppe):
                        spectrum[iw,ik] = np.abs(np.imag(self.experiment.intermediate_state.value(bzp-k + tiny, bzp-k + tiny, bzp-k+tiny,w)))
                    else:
                        print("experiment has no intermediate state!")
                        return
                elif mode == 'all':
                    if isinstance(self.experiment,Tppe):
                        spectrum[iw,ik] = self.experiment.spectral_function.value(bzp-k + tiny, bzp-k + tiny, bzp-k+tiny,w) +np.abs(np.imag(self.experiment.intermediate_state.value(bzp-k + tiny, bzp-k + tiny, bzp-k+tiny,w)))
                    else:
                        spectrum[iw,ik] = self.experiment.spectral_function.value(bzp-k + tiny, bzp-k + tiny, bzp-k+tiny,w)
                else:
                    spectrum[iw,ik] = self.experiment.spectral_function.value(bzp-k + tiny, bzp-k + tiny, bzp-k+tiny,w)

            for ik, k in enumerate(np.linspace(0., 1.5*bzp, n_gk)):
                if mode == 'initial':
                    spectrum[iw,ik+n_lg] = self.experiment.spectral_function.value(tiny+k, tiny+k, tiny,w)
                elif mode == 'intermediate':
                    if isinstance(self.experiment,Tppe):
                        spectrum[iw,ik+n_lg] = np.abs(np.imag(self.experiment.intermediate_state.value(tiny+k, tiny+k, tiny,w)))
                    else:
                        print("experiment has no intermediate state!")
                        return
                elif mode == 'all':
                    if isinstance(self.experiment,Tppe):
                        spectrum[iw,ik+n_lg] = self.experiment.spectral_function.value(tiny+k, tiny+k, tiny,w) +np.abs(np.imag(self.experiment.intermediate_state.value(tiny+k, tiny+k, tiny,w)))
                    else:
                        spectrum[iw,ik+n_lg] = self.experiment.spectral_function.value(tiny+k, tiny+k, tiny,w)
                else:
                    spectrum[iw,ik+n_lg] = self.experiment.spectral_function.value(tiny+k, tiny+k, tiny,w)


        # restore the surface, ulgy solution, has to be updated!!!
        self.experiment.spectral_function.surface = backup_surface

        end = dti.now()
        print('\r' + self.time_stamp_str() + "  " + "100.00% completed")
        total_time = str(end-start)
        print("total time: " + total_time)

        # save data
        function_tag =  "spectrum_fcc"
        data_type = ".txt"
        filename = self.generate_filename(sdir, function_tag, data_type)
        if filename != "":
            # save function parameters and spectrum
            header = []
            header.append("{}={:d}{}".format("n",n,os.linesep))
            header.append("{}={:.2f}{}".format("dk",dk,os.linesep))
            header.append("{}={:.2f}{}".format("be_min",be_min,os.linesep))
            header.append("{}={:.2f}{}".format("be_max",be_max,os.linesep))
            header.append("{}={:.2f}{}".format("lc",lc,os.linesep))
            header.append("{}={}{}".format("mode",mode,os.linesep))
            header.append("{}={}".format("total time",total_time))
            np.savetxt(filename, spectrum, fmt='%1.5e', header = "".join(header))

        return spectrum


    def bandstructure_fcc(self,dk=0.4,lc=0.41,sdir=""):
        """
        Calculate and return the bandstructure along high symmetry points
        G-X-W-L-G-K, as in Laesser, Smith PRB 24 (1981)

        To do: save parameters

        Keyword arguments:
        -----------------------------------------------------------------------
        dk (float):     k-resolution [nm^-1], e.g. (2 pi/a) / 100 ~ 0.15
        sdir (str):     name of the save directory, if sdir == "",
                        nothing will be saved
        lc (float):     lattice constant in nm
        """

        # note: for the simple tight binding models, the lattice parameter is
        # directly implemented in EVC, for the CIS it`s in the parameters class

        lattice_param = lc
        bzp = np.pi/lattice_param

        l_gx = 2.*bzp
        l_xw = bzp
        l_wl = np.sqrt(2.)*bzp
        l_lg = np.sqrt(3.)*bzp
        l_gk = 3.*np.sqrt(1./2.)*bzp

        n_gx = int(l_gx/dk)
        n_xw = int(l_xw/dk)
        n_wl = int(l_wl/dk)
        n_lg = int(l_lg/dk)
        n_gk = int(l_gk/dk)

        band_dim = self.experiment.spectral_function.evc.dim
        k_dim = n_gx + n_xw + n_wl + n_lg + n_gk

        bandstructure = np.zeros((band_dim, k_dim))

        #N = float(n)
        #progress = 0.
        start = dti.now()


        # G-X
        for ik, k in enumerate(np.linspace(0., 2.*bzp,n_gx)):
            try:
                eigv = self.experiment.spectral_function.evc.eigenenergies(tiny, tiny+k, tiny)
            except:
                eigv = np.zeros(band_dim)

            for idev,ev in enumerate(eigv):
                bandstructure[idev,ik] = ev


        # X-W
        for ik, k in enumerate(np.linspace(0., bzp, n_xw)):
            try:
                eigv = self.experiment.spectral_function.evc.eigenenergies(tiny+k, 2.*bzp, tiny)
            except:
                eigv = np.zeros(band_dim)

            for idev,ev in enumerate(eigv):
                bandstructure[idev,ik+n_gx] = ev

        #W-L
        for ik, k in enumerate(np.linspace(0., bzp, n_wl)):
            try:
                eigv = self.experiment.spectral_function.evc.eigenenergies(bzp, 2.*bzp-k, tiny+k)
            except:
                eigv = np.zeros(band_dim)

            for idev,ev in enumerate(eigv):
                bandstructure[idev,ik+n_gx+n_xw] = ev

        # L-G
        for ik, k in enumerate(np.linspace(0., bzp, n_lg)):
            try:
                eigv = self.experiment.spectral_function.evc.eigenenergies(bzp-k + tiny, bzp-k + tiny, bzp-k+tiny)
            except:
                eigv = np.zeros(band_dim)

            for idev,ev in enumerate(eigv):
                bandstructure[idev,ik + n_gx + n_xw + n_wl] = ev

        # G-K
        for ik, k in enumerate(np.linspace(0., 1.5*bzp, n_gk)):
            try:
                eigv = self.experiment.spectral_function.evc.eigenenergies(tiny + k, tiny+k, tiny)
            except:
                eigv = np.zeros(band_dim)

            for idev,ev in enumerate(eigv):
                bandstructure[idev,ik + n_gx + n_xw + n_wl + n_lg] = ev

        end = dti.now()
        print('\r' + self.time_stamp_str() + "  " + "100.00% completed")
        total_time = str(end-start)
        print("total time: " + total_time)

        # save data
        function_tag =  "bandstructure_fcc"
        data_type = ".txt"
        filename = self.generate_filename(sdir, function_tag, data_type)
        if filename != "":
            # save function parameters and spectrum
            header = []
            header.append("{}={:.2f}{}".format("dk",dk,os.linesep))
            header.append("{}={:.2f}{}".format("lc",lc,os.linesep))
            #header.append("{}={}{}".format("mode",mode,os.linesep))
            header.append("{}={}".format("total time",total_time))
            np.savetxt(filename, bandstructure, fmt='%1.5e', header = "".join(header))

        return bandstructure



    """ EXPERIMENTAL """
    def bandstructure_sdos(self,n=50,kz=0.,kmax=21):
        """
        Calculate the bandstructure along high symmetry points
        G-X-W-L-G-K, as in Laesser, Smith PRB 24 (1981)

        To do: save parameters

        Keyword arguments:
        -----------------------------------------------------------------------
        dk (float):     k-resolution [nm^-1], e.g. (2 pi/a) / 100 ~ 0.15
        sdir (str):     name of the save directory, if sdir == "",
                        nothing will be saved
        lc (float):     lattice constant in nm
        """

        bandstructure = np.zeros((self.experiment.spectral_function.evc.dim, n))

        # G-X
        for ik, kx in enumerate(np.linspace(-kmax, kmax,n)):
            kxn, kyn, kzn = align_surface(kx,0,kz,self.experiment.spectral_function.surface)
            try:
                eigv = self.experiment.spectral_function.evc.eigenenergies(kxn,kyn,kzn)
            except:
                eigv = np.zeros(self.evc.dim)

            for idev,ev in enumerate(eigv):
                bandstructure[idev,ik] = ev
        return bandstructure



    def spectrum_sdos_edc(self,n=50,kmax=21.,be_min=7.,be_max=0.,rot_angle=90.,
                          m=21,kz_min=0., kz_max=20.,mode='all', sdir=""):
        """
        Calculate and return an energy distribution cut (edc)
        for the projected (kz-integrated) surface density of states (sdos)

        Keyword arguments:
        -----------------------------------------------------------------------
        n (int):            integer resolution
        kmax (float):       maximum k_parallel value in nm^-1
        be_min (float):     minimum binding energy below the fermi level
                            (note: defined to be a positive number!)
        be_max (float):     maximum binding energy above the fermi level
                            (also a positive number)
        rot_angle (float):  azimuthal (or polar) angle for the cut
        m (int):            number of kz values in the range [kz_min,kz_max]
        kz_min (float):     minimum perpendicular momentum value
        kz_max (float):     maximum perpendicular momentum value
        mode (str):         valid options: 'all','initial','intermediate'
        sdir (str):         name of the save directory, if sdir == "",
                            nothing will be saved
        """

        Eaxis = np.linspace(-be_min,be_max,n)
        kaxis = np.linspace(-kmax,kmax,n)
        kzaxis = np.linspace(kz_min,kz_max,m)

        spectrum = np.zeros((n,n),dtype = float)
        N = float(n*n)

        #convert from degree to rad
        delta = rot_angle*np.pi/180.

        progress = 0.
        start = dti.now()

        #calulcate spectrum
        for iw,w in enumerate(Eaxis[::-1]):
            for ik,k in enumerate(kaxis):
                kxn,kyn = self.polar_rot(k,0.,delta)

                # sum up all kz-values
                spec = 0.
                for kz in kzaxis:
                    if mode == 'initial':
                        spec += self.experiment.spectral_function.value(kxn, kxn, kz,w)
                    elif mode == 'intermediate':
                        if isinstance(self.experiment,Tppe):
                            spec += np.abs(np.imag(self.experiment.intermediate_state.value(kxn, kxn, kz,w)))
                        else:
                            print("experiment has no intermediate state!")
                            return
                    elif mode == 'all':
                        if isinstance(self.experiment,Tppe):
                            spec += self.experiment.spectral_function.value(kxn, kxn, kz,w) +np.abs(np.imag(self.experiment.intermediate_state.value(kxn, kxn, kz,w)))
                        else:
                            spec += self.experiment.spectral_function.value(kxn, kxn, kz,w)
                    else:
                        spec += self.experiment.spectral_function.value(kxn, kxn, kz,w)

                spectrum[iw,ik] = spec


                #print progress on terminal
                progress = self.update_progress((iw*ik)/N,progress)

        end = dti.now()
        print('\r' + self.time_stamp_str() + "  " + "100.00% completed")
        total_time = str(end-start)
        print("total time: " + total_time)

        #save data
        function_tag =  "spectrum_sdos_edc"
        data_type = ".txt"
        filename = self.generate_filename(sdir, function_tag, data_type)
        if filename != "":
            # save function parameters and spectrum
            header = []
            header.append("{}={:d}{}".format("n",n,os.linesep))
            header.append("{}={:d}{}".format("m",m,os.linesep))
            header.append("{}={:.2f}{}".format("kmax",kmax,os.linesep))
            header.append("{}={:2f}{}".format("kz_min",kz_min,os.linesep))
            header.append("{}={:2f}{}".format("kz_max",kz_max,os.linesep))
            header.append("{}={:.2f}{}".format("be_min",be_min,os.linesep))
            header.append("{}={:.2f}{}".format("be_max",be_max,os.linesep))
            header.append("{}={:.2f}{}".format("rot_angle",rot_angle,os.linesep))
            header.append("{}={}{}".format("method",method,os.linesep))
            header.append("{}={}{}".format("mode",mode,os.linesep))
            header.append("{}={}".format("total time",total_time))
            np.savetxt(filename, spectrum, fmt='%1.5e', header = "".join(header))

            #save model parameters
            model_name = self.experiment.spectral_function.evc.model_name
            params = self.experiment.spectral_function.evc.params_str()
            self.save_params_str(sdir,model_name,params)

        return spectrum





    """
    ===========================================================================
                                PLOTTING
    ===========================================================================
    """

    def mdc_plot(self,data,kmax=21.,sdir="",**kwargs):
        """
        Plot an energy distribution cut (edc)(conventional ARPES spectrum)
        and save the figure optionally

        Keyword arguments:
        -----------------------------------------------------------------------
        data (2d array):    2D MDC spectrum
        kmax (float):       maximum k_parallel value in nm^-1
        sdir (str):         name of the save directory, if sdir == "",
                            nothing will be saved
        kwargs:             additional imshow parameters to format the plot
        """
        fig = plt.figure(figsize=(FIG_DIM_X, FIG_DIM_Y))
        mp = kmax*0.1
        #plt.contour(data,extent=[-mp,mp,-mp,mp])
        plt.imshow(data,extent=[-mp,mp,-mp,mp],cmap=self.cmap,**kwargs)
        plt.xlabel("k$_\mathsf{x}$ [$\AA^{-1}$]",fontsize=LABEL_SIZE)
        plt.ylabel("k$_\mathsf{y}$ [$\AA^{-1}$]",fontsize=LABEL_SIZE)

        #save figure
        function_tag =  "mdc"
        data_type = ".png"
        filename = self.generate_filename(sdir, function_tag, data_type)
        if filename != "":
            fig.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=TP_FLAG)

        plt.show()




    def edc_plot(self,data,kmax=21.,be_min=7,be_max=0,sdir="",
                 escale="",**kwargs):
        """
        Plot an energy distribution cut (edc)(conventional ARPES spectrum)
        and save the figure optionally

        Keyword arguments:
        -----------------------------------------------------------------------
        data (2d array):    2D EDC spectrum
        kmax (float):       maximum k_parallel value in nm^-1
        be_min (float):     minimum binding energy below the fermi level
                            (note: defined to be a positive number!)
        be_max (float):     maximum binding energy above the fermi level
                            (also a positive number)
        sdir (str):         name of the save directory, if sdir == "",
                            nothing will be saved
        escale (str):       energy scale, valid options are: "initial",
                            "intermediate","final"
        kwargs:             additional imshow parameters to format the plot
        """
        fig = plt.figure(figsize=(FIG_DIM_X, FIG_DIM_Y))
        mp = kmax*0.1
        #plt.contour(data,extent=[-mp,mp,-mp,mp])
        pe = self.experiment.light_source.central_energy

        if escale=="intermediate":
            # turn binding energy into intermediate state energy (be + photon energy)
            escale_min = -be_min+pe
            escale_max = be_max+pe
            escale_label = "E$_\mathsf{intermediate}$ [eV]"
        elif escale == "final":
            # turn binding energy into final state energy (be + photon energy*2)
            escale_min = -be_min+pe*2.
            escale_max = be_max+pe*2.
            escale_label = "E$_\mathsf{final}$ [eV]"
        elif escale == "initial":
            escale_min = -be_min
            escale_max = be_max
            escale_label = "E-E$_\mathsf{F}$ [eV]"

        else:
            escale_min = -be_min
            escale_max = be_max
            escale_label = "E-E$_\mathsf{F}$ [eV]"

        plt.imshow(data,extent=[-mp,mp,escale_min,escale_max],cmap=self.cmap,**kwargs)

        plt.xlabel("k$_\mathsf{||}$ [$\AA^{-1}$]",fontsize=LABEL_SIZE)
        plt.ylabel(escale_label,fontsize=LABEL_SIZE)

        # save figure
        function_tag = "edc"
        data_type = ".png"
        filename = self.generate_filename(sdir, function_tag, data_type)
        if filename != "":
            fig.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=TP_FLAG)

        plt.show()


    def spectrum_sdos_plot(self,data,kmax=21.,be_min=7,be_max=0,
                           sdir="",**kwargs):
        """
        Plot an energy distribution cut (edc) for the projected
        (kz-integrated) surface density of states (sdos)

        Keyword arguments:
        -----------------------------------------------------------------------
        data (2d array):    2D projected surface DOS EDC spectrum
        kmax (float):       maximum k_parallel value in nm^-1
        be_min (float):     minimum binding energy below the fermi level
                            (note: defined to be a positive number!)
        be_max (float):     maximum binding energy above the fermi level
                            (also a positive number)
        sdir (str):         name of the save directory, if sdir == "", nothing
                            will be saved
        kwargs:             additional imshow parameters to format the plot
        """
        fig = plt.figure(figsize=(FIG_DIM_X, FIG_DIM_Y))
        mp = kmax*0.1
        #plt.contour(data,extent=[-mp,mp,-mp,mp])
        plt.imshow(data,extent=[-mp,mp,-be_min,be_max],cmap=self.cmap,**kwargs)
        plt.xlabel("k$_\mathsf{||}$ [$\AA^{-1}$]",fontsize=LABEL_SIZE)
        plt.ylabel("E-E$_\mathsf{F}$ [eV]",fontsize=LABEL_SIZE)

        # save figure
        function_tag = "spectrum_sdos"
        data_type = ".png"
        filename = self.generate_filename(sdir, function_tag, data_type)
        if filename != "":
            fig.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=TP_FLAG)

        plt.show()


    def spectrum_fcc_plot(self,data,dk=0.4,be_min=7,be_max=1,lc=0.41,
                          sdir='',**kwargs):
        """
        Plot the spectrum of initial/intermediate states along high symmetry
        points of an FCC crystal: G-X-W-L-G-K, as in
        Laesser, Smith PRB 24 (1981)

        Keyword arguments:
        -----------------------------------------------------------------------
        data (2d array):    spectrum returned from spectrum_fcc()
        dk (float):         momentum resolution [nm^-1],
                            e.g. (2 pi/a) / 100 ~ 0.15
        be_min (float):     minimum binding energy below the fermi level
                            (note: defined to be a positive number!)
        be_max (float):     maximum binding energy above the fermi level
                            (also a positive number)
        lc (float):         lattice constant in nm
        sdir (str):         name of the save directory, if sdir == "",
                            nothing will be saved
        kwargs:             additional imshow parameters to format the plot
        """

        lattice_param = lc
        bzp = np.pi/lattice_param

        l_gx = 2.*bzp
        l_xw = bzp
        l_wl = np.sqrt(2.)*bzp
        l_lg = np.sqrt(3.)*bzp
        l_gk = 3.*np.sqrt(1./2.)*bzp

        n_gx = int(l_gx/dk)
        n_xw = int(l_xw/dk)
        n_wl = int(l_wl/dk)
        n_lg = int(l_lg/dk)
        n_gk = int(l_gk/dk)

        k_dim = n_gx + n_xw + n_wl + n_lg + n_gk

        fig = plt.figure(figsize=(FIG_DIM_X*2, FIG_DIM_Y))
        #fig,ax = plt.subplots()


        plt.imshow(np.flipud(np.real(data)),cmap=self.cmap,aspect=5,extent=[0,n_gx+n_xw+n_wl+n_lg+n_gk,-be_min,be_max],**kwargs);

        wid = 2.
        plt.vlines(n_gx,-be_min,be_max,color = BOUNDARY_COLOR,lw=wid)
        plt.vlines(n_xw+n_gx,-be_min,be_max,color = BOUNDARY_COLOR,lw=wid)
        plt.vlines(n_wl+n_xw+n_gx,-be_min,be_max,color = BOUNDARY_COLOR,lw=wid)
        plt.vlines(n_lg+n_wl+n_xw+n_gx,-be_min,be_max,color = BOUNDARY_COLOR,lw=wid)
        plt.hlines(0.,0,k_dim,lw=1.5,linestyle="--",color=EFERMI_COLOR)

        plt.xlim(0,k_dim)
        plt.ylim(-be_min,be_max)
        #yticks(np.linspace(-2,18.,9))
        plt.ylabel("E-E$_\mathsf{F}$ [eV]",fontsize=LABEL_SIZE)
        plt.tick_params(axis='both', which='major', labelsize=16)
        #title(plottitle,fontsize = TITLE_SIZE,weight=TITLE_WEIGHT)
        plt.xlabel("Reduced wave vector",fontsize = 16,weight='medium')

        plt.xticks([0,n_gx, n_gx+n_xw, n_gx+n_xw+n_wl, n_gx+n_xw+n_wl+\
                    n_lg,n_gx+n_xw+n_wl+n_lg+n_gk],\
                   [r'$\Gamma$',r"$X$", r"$W$", r"$L$",r"$\Gamma$",r"$K$"])

        # save figure
        function_tag =  "spectrum_fcc"
        data_type = ".png"
        filename = self.generate_filename(sdir, function_tag, data_type)
        if filename != "":
            fig.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=TP_FLAG)

        plt.show()



    def spectrum_fcc_lgk_plot(self,data,dk=0.4,be_min=7,be_max=1,lc=0.41,
                              sdir='',**kwargs):
        """
        Plot the spectrum of initial/intermediate states along
        high symmetry points of an FCC crystal: L-G-K

        Keyword arguments:
        -----------------------------------------------------------------------
        data (2d array):    spectrum returned from spectrum_fcc()
        dk (float):         momentum resolution [nm^-1],
                            e.g. (2 pi/a) / 100 ~ 0.15
        be_min (float):     minimum binding energy below the fermi level
                            (note: defined to be a positive number!)
        be_max (float):     maximum binding energy above the fermi level
                            (also a positive number)
        lc (float):         lattice constant in nm
        sdir (str):         name of the save directory, if sdir == "",
                            nothing will be saved
        kwargs:             additional imshow parameters to format the plot
        """

        lattice_param = lc
        bzp = np.pi/lattice_param

        l_lg = np.sqrt(3.)*bzp
        l_gk = 3.*np.sqrt(1./2.)*bzp

        n_lg = int(l_lg/dk)
        n_gk = int(l_gk/dk)

        k_dim = n_lg + n_gk

        fig = plt.figure(figsize=(FIG_DIM_X*2, FIG_DIM_Y))
        #fig,ax = plt.subplots()


        plt.imshow(np.flipud(np.real(data)),cmap=self.cmap,aspect=5,extent=[0,k_dim,-be_min,be_max],**kwargs);

        wid = 2.
        #plt.vlines(n_gx,-be_min,be_max,color = BOUNDARY_COLOR,lw=wid)
        #plt.vlines(n_xw+n_gx,-be_min,be_max,color = BOUNDARY_COLOR,lw=wid)
        #plt.vlines(n_wl+n_xw+n_gx,-be_min,be_max,color = BOUNDARY_COLOR,lw=wid)
        plt.vlines(n_lg,-be_min,be_max,color = BOUNDARY_COLOR,lw=wid)
        plt.hlines(0.,0,k_dim,lw=1.5,linestyle="--",color=EFERMI_COLOR)

        plt.xlim(0,k_dim)
        plt.ylim(-be_min,be_max)
        #yticks(np.linspace(-2,18.,9))
        plt.ylabel("E-E$_\mathsf{F}$ [eV]",fontsize=LABEL_SIZE)
        plt.tick_params(axis='both', which='major', labelsize=16)
        #title(plottitle,fontsize = TITLE_SIZE,weight=TITLE_WEIGHT)
        plt.xlabel("Reduced wave vector",fontsize = 16,weight='medium')

        plt.xticks([0,n_lg,n_lg+n_gk],[r"$L$",r"$\Gamma$",r"$K$"])

        # save figure
        function_tag =  "spectrum_fcc_lgk"
        data_type = ".png"
        filename = self.generate_filename(sdir, function_tag, data_type)
        if filename != "":
            fig.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=TP_FLAG)

        plt.show()



    def bandstructure_fcc_plot(self, data, dk=0.4, be_min=7.,
                               be_max=0.,lc=0.41,sdir=""):
        """
        Plot and save a bandstructure overview calculation along the path
        G-X-W-L-G-K, as in Laesser, Smith PRB 24 (1981)

        To do:
            + extend to allow to plot intermediate state bandstructure
            + save parameters

        Keyword arguments:
        -----------------------------------------------------------------------
        data (2d array):           result of a bandstructure function
        dk (float):                k-resolution [nm^-1],
                                   e.g. (2 pi/a) / 100 ~ 0.15
        be_min (float):            minimum binding energy below the fermi level
                                   (note: defined to be a positive number!)
        be_max (float):            maximum binding energy above the fermi level
                                   (also a positive number)
        sdir (str):                name of the save directory, if sdir == "",
                                   nothing will be saved
        lc (float):                lattice constant in nm
        """

        # note: for the simple tight binding models, the lattice parameter is
        # directly implemented in EVC, for the CIS it`s in the parameters class
        lattice_param = lc
        bzp = np.pi/lattice_param

        l_gx = 2.*bzp
        l_xw = bzp
        l_wl = np.sqrt(2.)*bzp
        l_lg = np.sqrt(3.)*bzp
        l_gk = 3.*np.sqrt(1./2.)*bzp

        n_gx = int(l_gx/dk)
        n_xw = int(l_xw/dk)
        n_wl = int(l_wl/dk)
        n_lg = int(l_lg/dk)
        n_gk = int(l_gk/dk)

        #band_dim = self.experiment.spectral_function.evc.dim
        k_dim = n_gx + n_xw + n_wl + n_lg + n_gk

        fig = plt.figure(figsize=(10,6))
        ax = fig.add_subplot(111)

        BOUNDARY_COLOR = "gray"
        BAND_COLOR = "darkblue"
        EDGE_COLOR = "darkblue"
        POINT_SIZE = 2.
        PLOT_STYLE = "o"


        for idband, band in enumerate(data):
            plt.plot(band,lw=1.5,c=BAND_COLOR)

        wid = 2.
        plt.vlines(n_gx,-be_min,be_max,color = BOUNDARY_COLOR,lw=wid)
        plt.vlines(n_xw+n_gx,-be_min,be_max,color = BOUNDARY_COLOR,lw=wid)
        plt.vlines(n_wl+n_xw+n_gx,-be_min,be_max,color = BOUNDARY_COLOR,lw=wid)
        plt.vlines(n_lg+n_wl+n_xw+n_gx,-be_min,be_max,color = BOUNDARY_COLOR,lw=wid)

        plt.xlim(0,k_dim)
        plt.ylim(-be_min, be_max)

        plt.ylabel('E-E$_\mathsf{_F}$ [eV]',fontsize=LABEL_SIZE,weight=LABEL_WEIGHT)
        plt.tick_params(axis='both', which='major', labelsize=16)
        #title(plottitle,fontsize = TITLE_SIZE,weight=TITLE_WEIGHT)
        plt.xlabel("Reduced wave vector",fontsize = LABEL_SIZE,weight=LABEL_WEIGHT)

        plt.xticks([0,n_gx, n_gx+n_xw, n_gx+n_xw+n_wl, n_gx+n_xw+n_wl+\
                    n_lg,n_gx+n_xw+n_wl+n_lg+n_gk],\
                   [r'$\Gamma$',r"$X$", r"$W$", r"$L$",r"$\Gamma$",r"$K$"])

        # save figure
        function_tag =  "bandstructure_fcc_lgk"
        data_type = ".png"
        filename = self.generate_filename(sdir, function_tag, data_type)
        if filename != "":
            fig.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=TP_FLAG)

        plt.show()


    """
    ===========================================================================
                                    SUPPORT
    ===========================================================================
    """

    @classmethod
    def generate_filename(cls,sdir,function_tag,data_type):
        """ Check and create directory and return a filename

        Keyword arguments:
        -----------------------------------------------------------------------
        sdir (str):         save directory
        function_tag (str): a string, identifying the calculation function
        data_type (str):    txt,png
        """

        if sdir != "":
            path = str(getcwd() + os.path.sep + sdir)
            filename = str(path + os.path.sep + function_tag + data_type)
            if not os.path.exists(path):
                os.makedirs(path)
            else:
                # if the directory already exists, check if the filename
                # already exists. if so, append a time stamp on the filename
                if os.path.exists(filename):
                    filename = str(getcwd() + os.path.sep + sdir) + os.path.sep + function_tag + "_" + cls.time_stamp_str() + data_type
            return filename
        else:
             return ""


    @staticmethod
    def polar_rot(kx,ky,angle):
        """Rotate k parallel pair and return a tuple"""
        return kx*np.cos(angle)-ky*np.sin(angle), kx*np.sin(angle)+ky*np.cos(angle)

    #@staticmethod
    #def detector_rot_x(kx,ky,kz,angle):
    #    return kx*np.cos(angle)+kz*np.sin(angle), ky, -kx*np.sin(angle)+kz*np.cos(angle)

    #@staticmethod
    #def detector_rot_y(kx,ky,kz,angle):
    #    return kx, ky*np.cos(angle)-kz*np.sin(angle), ky*np.sin(angle)+kz*np.cos(angle)


    def save_params_str(self,sdir,model_name,params_str):
        """
        Save a parameter string to a textfile

        To do:
            + update parameters, fermi energy and workfunction moved to
              experiment class

        Keyword arguments:
        -----------------------------------------------------------------------
        sdir (str):         save directory
        model_name (str):   model name of the corresponding EVC
        params_str (str):   parameter string returned by the corresponding EVC
        """
        """ TO DO: to be Linux and Windows compatible you have to use os.path.sep instead of / """
        filename = str(getcwd() + os.path.sep + sdir + os.path.sep + model_name + ".txt")
        if os.path.exists(filename):
            filename = str(str(getcwd() + os.path.sep + sdir) + os.path.sep + model_name + "_" + self.time_stamp_str() + ".txt")

        params_file = open(filename, "w")
        #params_file.write("{}={:.2f}\n".format("fermi_energy",self.experiment.fermi_energy))
        #params_file.write("{}={:.2f}\n".format("workfunction",self.experiment.workfunction))
        params_file.write(self.experiment.params_str())
        params_file.write(params_str)
        params_file.close()
    #    else:

    @classmethod
    def update_progress(cls,n,progress):
        """Print percental progress, used in edc(), mdc(), ..."""
        if(n >= progress):
            os.system('clear')
            sys.stdout.write("\r")
            sys.stdout.write(cls.time_stamp_str() + "  " + "{:2.2f}".format(progress*100) + "% completed");
            #flush command gives error when module is used with MATLAB
            #sys.stdout.flush()
            progress = progress + 0.01
        return progress

    @classmethod
    def time_stamp_str(cls):
        """Return datetime without milliseconds"""
        return dti.now().strftime("%Y-%m-%d_%H:%M:%S")




if __name__ == "__main__":
    pass
