# This file is part of PECMAN

# PECMAN is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from pecman.vector3d import *

class Quaternion():

    def __init__(self):
        self.i = Vector3D(0.,0.,0.)
        self.r = 1.

    def identity(self):
        self.i.x = 0.
        self.i.y = 0.
        self.i.z = 0.
        self.r = 1.

    def length(self):
        return np.sqrt(self.i.x* self.i.x + self.i.y*self.i.y + self.i.z*self.i.z + self.r *self.r)

    def normalize(self):
        l = self.length()
        if l != 0.:
            self.i.x /= l
            self.i.y /= l
            self.i.z /= l
            self.r /= l

    def conjugate(self):
        self.i.invert()

    def invert(self):
        self.conjugate()
        self.normalize()

    def add(self, quat):
        self.i.add(quat.i)
        self.r += quat.r

    def mulq(self, q):
        res = Quaternion()
        res.i.x = self.r * q.i.x + self.i.x * q.r   + self.i.y * q.i.z - self.i.z * q.i.y
        res.i.y = self.r * q.i.y - self.i.x * q.i.z + self.i.y * q.r   + self.i.z * q.i.x
        res.i.z = self.r * q.i.z + self.i.x * q.i.y - self.i.y * q.i.x + self.i.z * q.r
        res.r   = self.r * q.r   - self.i.x * q.i.x - self.i.y * q.i.y - self.i.z * q.i.z
        return res

    def mul(self, q):
        self.copy(self.mulq(q))

    def copy(self,q):
        self.i.x = q.i.x
        self.i.y = q.i.y
        self.i.z = q.i.z
        self.r = q.r

    def build_rotation(self,v,theta):
        res = Quaternion()
        res.i.x = v.x * np.sin(theta/2.)
        res.i.y = v.y * np.sin(theta/2.)
        res.i.z = v.z * np.sin(theta/2.)
        res.r = np.cos(theta/2.)
        return res

    def to_matrix(self):
        m = np.zeros((4,4),dtype=complex)
        m[0,0] = 1. - 2. * self.i.y * self.i.y - 2. * self.i.z * self.i.z
        m[0,1] = 2. * self.i.x * self.i.y + 2. * self.r * self.i.z
        m[0,2] = 2. * self.i.x * self.i.z - 2. * self.r * self.i.y
        m[0,3] = 0.

        m[1,0] = 2. * self.i.x * self.i.y - 2. * self.r * self.i.z
        m[1,1] = 1. - 2. * self.i.x * self.i.x - 2. * self.i.z * self.i.z
        m[1,2] = 2. * self.i.y * self.i.z + 2. * self.r * self.i.x
        m[1,3] = 0.

        m[2,0] = 2. * self.i.x * self.i.z + 2. * self.r * self.i.y
        m[2,1] = 2. * self.i.y * self.i.z - 2. * self.r * self.i.x
        m[2,2] = 1. - 2. * self.i.x * self.i.x - 2. * self.i.y * self.i.y
        m[2,3] = 0.

        m[3,0] = 0.
        m[3,1] = 0.
        m[3,2] = 0.
        m[3,3] = 1.

        return m
