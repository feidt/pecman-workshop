# This file is part of PECMAN

# PECMAN is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np

class Vector3D():

    def __init__(self,x=0.,y=0.,z=0.,w=0.):
        self.x = x
        self.y = y
        self.z = z
        self.w = w

    def zero(self):
        self.x = 0.
        self.y = 0.
        self.z = 0.
        self.w = 0.

    def add(self, vec):
        self.x += vec.x
        self.y += vec.y
        self.z += vec.z

    def sub(self, vec):
        self.x -= vec.x
        self.y -= vec.y
        self.z -= vec.z

    def mul(self, vec):
        self.x *= vec.x
        self.y *= vec.y
        self.z *= vec.z

    def invert(self, vec):
        self.x = -self.x
        self.y = -self.y
        self.z = -self.z

    def scale(self, f):
        self.x *= f
        self.y *= f
        self.z *= f

    def length(self):
        return np.sqrt(self.x*self.x + self.y+self.y+self.z+self.z)

    def dot(self, vec):
        return self.x * vec.x + self.y * vec.y + self.z * self.z

    def normalize(self):
        l = self.length()
        if l != 0.:
            self.x /= l
            self.y /= l
            self.z /= l
        self.w = 1.

    def copy(self,vec):
        self.x = vec.x
        self.y = vec.y
        self.z = vec.z
        self.w = vec.w

    def cross(self, vec):
        nvec = Vector3D(self.y*vec.z - self.z*vec.y, self.z*vec.x - self.x*vec.z, self.x*vec.y - self.y*vec.x, 1.)
        return nvec
