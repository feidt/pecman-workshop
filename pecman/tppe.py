# This file is part of PECMAN

# PECMAN is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from scipy.integrate import quad

from pecman.experiment import Experiment
from pecman.phelper import *


class Tppe(Experiment):

    # experiment identifyer
    exptype = "tppe"

    def __init__(self,spectral_function,intermediate_state,final_state,
                 light_source,fermi_energy=0.,workfunction=4.5):
        """
        Create an object for describing an angle-resolved 2PPE experiment
        j2ppe ~ Grf Gaf Gri Gai G< A_L
              ~ final_state*intermediate_state*initial_state*light_source

        Attributes:
        -----------------------------------------------------------------------
        spectral_function
        (SpectralFunction):     representation of the initial state entering
                                via G<
        intermediate_state
        (IntermediateState):    representation of the intermediate state
                                entering via Gr*Ga
        final_state
        (FinalState):           representation of the final state entering via
                                Gr*Ga, e.g. a ILEEDState,...
        light_source
        (LightSource):          CWSource, Laser, ...
        fermi_energy (float):   Fermi energy of the system [eV]. If set to 0,
                                every binding energy is relative to Ef
        workfunction (float):   The workfunction of the material [eV]
        """
        #self.fermi_energy = fermi_energy
        #self.workfunction = workfunction
        Experiment.__init__(self,fermi_energy,workfunction)
        #self.alpha =  0.0381 #hbar*hbar/2m_electron, [eV nm**2]
        self.spectral_function = spectral_function
        self.intermediate_state = intermediate_state
        self.final_state = final_state
        self.light_source = light_source



    """
    ===========================================================================
                                PHOTOEMISSION SIGNAL
    ===========================================================================
    """

    # photoemission kernel for two-photon-photoemission
    def pes_kernel(self,photonenergy,kx,ky,w):
        """
        Return the integration kernel of the photoemission signal for
        method = 'em_conserved'. If the light source has a spectral width,
        the spectral function is convoluted with the light source

        Keyword arguments:
        -----------------------------------------------------------------------
        photonenergy (float):   photon energy in [eV]
        kx,ky (float):          parallel momentum components [nm^-1]
        w (float):              kinetic energy [eV]
        """
        #integral kernel dx ~ Efield(ekin-x)*Asf(kx,ky,kz,x) dx

        # perpenticular wavevector inside the solid
        kg = np.real(self.final_state.get_ks(kx,ky,w))
        kgi = np.imag(self.final_state.get_ks(kx,ky,w))

        # perpendicular wavevector inside the vacuum
        kd = np.real(self.final_state.get_kv(kx,ky,w))

        # free electron cone * fermi_function * positive kinetic energy
        f0 = th(w-alpha*(kx**2+ky**2))*th( -self.workfunction - w + photonenergy*2.)*th(w)

        # preliminary final state imaginary part of the perpendicular momentum
        k2 = 0.1+kgi
        # final contribution (Gr*Ga)
        f1 = kd/((kd+kg)**2+k2**2)*1./(2.*k2)

        # contribution of the light source
        f2 = self.light_source.value(photonenergy)

        # photohole contribution = spectral function: evaluated at k-perpendicular of the final state
        # inside the solid
        f3 = self.spectral_function.value(kx,ky,kg,w-photonenergy*2.+self.fermi_energy+self.workfunction)

        # intermediate state contribution
        # at present, kz=0, a perfect 2D state
        """ check sign of imagkz, it has to be negative (probably) """
        f4 = self.intermediate_state.value(kx,ky,kg,w-photonenergy+self.fermi_energy+self.workfunction)

        kernel = f0*f1*np.abs(f2)**4*f3 *np.conjugate(f4)*f4
        return np.real(kernel)


    def pes(self,kx,ky,w,method='em_conserved'):
        """
        Calculate and return the photoemission signal

        To do: laser integration and method=kz_integrated

        Keyword arguments:
        -----------------------------------------------------------------------
        kx,ky (float):      parallel momentum components
        w (float):          kinetic energy
        method (str):       options = ['em_conserved','kz_integrated']
        """
        if method == 'em_conserved':

            """
            # check if light source is laser or cw
            if hasattr(self.light_source,'spectral_width'):
                # evalute integral only around the current kinetic energy. other regions should give negligible contributions...
                # check if integral evaluation point (w dependency) is correct?
                lower_bound = w-self.light_source.central_energy - self.light_source.spectral_width*2.
                upper_bound = w-self.light_source.central_energy + self.light_source.spectral_width*2.
                result = quad(self.pes_kernel,lower_bound,upper_bound,args=(kx,ky,w,surface))[0]

            else:
            """
            """ TO DO: change Arpes class to same parameter transfer,i.e. instead of x=w-Omega -> Omega and adjust in pes_kernel function ... """
            result = self.pes_kernel(self.light_source.central_energy,kx,ky,w)


        if(np.isnan(result)):
            return -1.
        else:
            return result


    def params_str(self):
        return ""
