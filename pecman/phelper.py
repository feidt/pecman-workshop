# This file is part of PECMAN

# PECMAN is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np

# photoemission kernel methods
METHOD2 = "kz_integrated"
METHOD1 = "em_conserved"

# a factor to prevent division by zero in bandstructure calculations
tiny = 1e-12

# free electron hbar**2/2m [nm^-2]
alpha =  0.0381

# Boltzmann constant
kb = 8.6173303e-5 # [eV/K]


def th(x):
    """Return the Heaviside step function"""
    return (1. if x > 0. else 0.)

def nf(x,T):
    """
    Return the Fermi distribution

    Keyword arguments:
    --------------------------------------
    x:          energy [eV]
    T:          temperature [K]
    """
    beta = 1./(T*kb)
    return 1./(np.exp(beta*x)+1.)


def align_surface(kx,ky,kz,surface):
    """To do: extend to general angles, right now just FCC surfaces"""
    #kx,ky,kz = kx*np.cos(polar_angle)+kz*np.sin(polar_angle),ky, kz*np.cos(polar_angle) - kx*np.sin(polar_angle)
    if surface == '111':
        kxn = (0.408248290463863* kx - 0.7071067811865475* ky + 0.5773502691896258* kz)
        kyn = (0.40824829046386296* kx + 0.7071067811865476* ky + 0.5773502691896257* kz)
        kzn = 0. - 0.816496580927726* kx + 0.5773502691896257* kz

    elif surface == '110':
        kxn = (0.408248290463863* kx - 0.5773502691896258* ky + 0.7071067811865475* kz)
        kyn = (0.+ 0.816496580927726* kx + 0.5773502691896258* ky)
        kzn = -0.408248290463863* kx + 0.5773502691896258* ky + 0.7071067811865475* kz

    else:
        # for FCC cystals, default: surface = '001'
        kxn = kx
        kyn = ky
        kzn = kz

    return kxn,kyn,kzn
