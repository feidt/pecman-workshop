# This file is part of PECMAN

# PECMAN is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np

class CombIntSchemeFCCParametersCu(object):

    def __init__(self):

        self.ensf = 1.2#1.4 #1.18

        self.bohr = 0.52917721092
        self.Ry = 13.605698066

        self.alpha = 0.01401*self.Ry *self.bohr*self.bohr
        self.S = 0.995*self.Ry
        self.R = 0.288 *self.bohr
        self.Bt = 1.664*self.Ry
        self.Be = 1.664*self.Ry
        self.a = 0.36147 # [nm]

        self.V000 = -0.07522*self.Ry
        self.V111 = 0.11392*self.Ry
        self.V200 = 0.11582*self.Ry
        self.V220 = 0.06780*self.Ry
        self.V311 = 0.01562*self.Ry
        self.V222 = 0.04804*self.Ry
        self.V331 = 0.
        self.V400 = 0.
        self.V420 = 0.

        self.A1 =  0.00614*self.Ry
        self.A2 = 0.00160*self.Ry
        self.A3 = 0.00433*self.Ry
        self.A4 = 0.00481*self.Ry
        self.A5 = 0.00066*self.Ry
        self.A6 = 0.00114*self.Ry
        self.E0 = 0.4033*self.Ry
        self.ED = -0.0028*self.Ry

        self.xi = 0.01*self.Ry

        self.lu = 2.*np.pi/self.a

        # these parameters are not from the original CIS but are for test purposes
        self.csd1 = 1.
        self.csd2 = 1.
        self.b = 1.
        self.dV = 0.
        self.dE = 0.
        self.dB1 = 1.
        self.dB2 = 1.
