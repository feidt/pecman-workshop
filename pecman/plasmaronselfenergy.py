# This file is part of PECMAN

# PECMAN is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from scipy.integrate import quad
from pecman.selfenergy import Selfenergy


""" EXPERIMENTAL """
class PlasmaronSelfenergy(Selfenergy):

    def __init__(self,workfunction=4.5,photon_energy=21.):
        self.fermi_energy = 7.
        self.wp = 6.6
        self.workfunction = workfunction
        self.photon_energy = photon_energy
        self.alpha =  0.0381
        self.kf = np.sqrt(self.fermi_energy/self.alpha)



    def value(self,kx,ky,kz,w):
        k = np.sqrt(kx**2 + ky**2 + kz**2)
        c = 1.
        i1r = quad(self.int1r,k+self.kf,50.,args=(k,w-( self.photon_energy-self.workfunction)))[0]
        i1i = quad(self.int1i,k+self.kf,50.,args=(k,w-(self.photon_energy-self.workfunction)))[0]

        i2r = quad(self.int2r,np.abs(self.kf-k),self.kf+k,args=(k,w-(self.photon_energy-self.workfunction)))[0]
        i2i = quad(self.int2i,np.abs(self.kf-k),self.kf+k,args=(k,w-(self.photon_energy-self.workfunction)))[0]

        i3r = quad(self.int3r,0.01,self.kf-k,args=(k,w-(self.photon_energy-self.workfunction)))[0]
        i3i = quad(self.int3i,0.01,self.kf-k,args=(k,w-(self.photon_energy-self.workfunction)))[0]

        i4r = quad(self.int4r,0.01,k-self.kf,args=(k,w-(self.photon_energy-self.workfunction)))[0]
        i4i = quad(self.int4i,0.01,k-self.kf,args=(k,w-(self.photon_energy-self.workfunction)))[0]

        re = c*self.wp**2*(-1./k * (i1r + i2r + i3r + i4r))
        im = c*self.wp**2*(-1./k * (i1i + i2i + i3i + i4i))

        return re +1j*im

    def theta(self,x):
        if x < 0.:
            return 0.
        else:
            return 1.

    """ check vf """
    def wq(self,q):
        return np.sqrt(self.wp**2 + self.alpha*q*q/3.) #+ self.alpha*self.alpha*q*q*q*q)

    def fe(self,q):
        return self.alpha*(q*q)-self.fermi_energy-1.5

    def int1r(self,q,k,w):
        return 1./(q*self.wq(q)) * np.log(np.abs((self.fe(k+q)-w+self.wq(q))/(self.fe(k-q)-w+self.wq(q))))
    def int1i(self,q,k,w):
        return -1./(q*self.wq(q))*np.pi*self.theta(self.fe(k+q) - w + self.wq(q)) * self.theta(w-self.wq(q)-self.fe(k-q))


    def int2r(self,q,k,w):
        return 1./(q*self.wq(q))*np.log(np.abs((self.fe(self.kf) - w - self.wq(q)) / (self.fe(k-q) - w - self.wq(q)) * (self.fe(k+q) - w + self.wq(q)) / (self.fe(self.kf) - w + self.wq(q))))
    def int2i(self,q,k,w):
        return 1./(q*self.wq(q)) * np.pi * (self.theta(self.fe(self.kf) - w - self.wq(q)) * self.theta( w + self.wq(q) - self.fe(k-q)) -\
                                    self.theta(self.fe(k+q) - w + self.wq(q)) * self.theta(w - self.wq(q) - self.fe(self.kf)))

    def int3r(self,q,k,w):
        return self.theta(self.kf-k) * 1./(q*self.wq(q)) *  np.log( np.abs( (self.fe(k+q) - w - self.wq(q) ) / ( self.fe(k-q) - w - self.wq(q))))
    def int3i(self,q,k,w):
        return self.theta(self.kf-k) * 1./(q*self.wq(q)) * np.pi * self.theta( self.fe(k+q) - w - self.wq(q)) * self.theta( w + self.wq(q) - self.fe(k-q))

    def int4r(self,q,k,w):
        return self.theta(k - self.kf) * 1./(q*self.wq(q)) * np.log( np.abs( (self.fe(k+q) - w + self.wq(q)) / (self.fe(k-q) - w + self.wq(q))))
    def int4i(self,q,k,w):
        return -self.theta(k - self.kf)/(q*self.wq(q))*np.pi*self.theta(self.fe(k+q) - w + self.wq(q)) * self.theta(w-self.wq(q)-self.fe(k-q))
