# This file is part of PECMAN

# PECMAN is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import numpy as np
import os
from pecman.phelper import *
from pecman.evc import EVC

class FreeElectron2DState(EVC):

    # a simple perfect two dimensional free electron state
    def __init__(self,alpha=0.0381, eps0=0.):
        """A two-dimensional free electron state"""
        self.dim = 1
        self.model_name = "FreeElectron2DState"
        self.alpha = alpha #hbar*hbar/2m_electron, [eV nm**2]
        self.eps0 = eps0

    def eigenenergies(self,kx,ky,kz):
        return [self.alpha*(kx**2+ky**2) + self.eps0]

    def params_str(self):
        pstr = []
        pstr.append("{}={:.2f}{}".format("model_name",self.model_name,os.linesep))
        pstr.append("{}={:.2f}{}".format("dim",self.dim,os.linesep))

        pstr.append("{}={:.2f}{}".format("alpha",self.alpha,os.linesep))
        pstr.append("{}={:.2f}{}".format("eps0",self.eps0,os.linesep))
        return "".join(pstr)



class FreeElectron3DState(EVC):
    # a simple perfect two dimensional free electron state
    def __init__(self,alpha=0.0381, eps0=0.):
        """A three-dimensional free electron state"""
        self.dim = 1
        self.model_name = "FreeElectron3DState"
        self.alpha =  alpha #hbar*hbar/2m_electron, [eV nm**2]
        self.eps0 = eps0


    def eigenenergies(self,kx,ky,kz):
        return [self.alpha*(kx**2+ky**2+kz**2) + self.eps0]

    def params_str(self):
        pstr = []
        pstr.append("{}={:.2f}{}".format("model_name",self.model_name,os.linesep))
        pstr.append("{}={:.2f}{}".format("dim",self.dim,os.linesep))

        pstr.append("{}={:.2f}{}".format("alpha",self.alpha,os.linesep))
        pstr.append("{}={:.2f}{}".format("eps0",self.eps0,os.linesep))
        return "".join(pstr)





class TightBindingFCC(EVC):
    def __init__(self,t=-0.35,a=0.41,eps0=-2.):
        """
        A single band tight binding model for FCC crystals with 12 nearest
        neighbors
        """
        self.dim = 1
        self.model_name = "TightBindingFCC"
        self.t = t
        self.a = a
        self.eps0 = eps0

    def eigenenergies(self,kx,ky,kz):
        """
        Return the momentum dependent eigenenergy, formulated in the
        coordinate system with kz || 001
        """
        return [self.eps0 + 4.*self.t*(np.cos(0.5*kx*self.a)*np.cos(0.5*ky*self.a)
                +np.cos(0.5*kx*self.a)*np.cos(0.5*kz*self.a)
                +np.cos(0.5*ky*self.a)*np.cos(0.5*kz*self.a))]


    def params_str(self):
        pstr = []
        pstr.append("{}={:.2f}{}".format("model_name",self.model_name,os.linesep))
        pstr.append("{}={:.2f}{}".format("dim",self.dim,os.linesep))

        pstr.append("{}={:.2f}{}".format("t",self.t,os.linesep))
        pstr.append("{}={:.2f}{}".format("a",self.a,os.linesep))
        pstr.append("{}={:.2f}{}".format("eps0",self.eps0,os.linesep))
        return "".join(pstr)


class TightBindingFCCrot(EVC):
    def __init__(self,t=-0.35,a=0.41,eps0=-2.):
        """
        A single band tight binding model for FCC crystals with 12 nearest
        neighbors
        """
        self.dim = 1
        self.model_name = "TightBindingFCCrot"
        self.t = t
        self.a = a
        self.eps0 = eps0

    def eigenenergies(self,kx,ky,kz):
        """
        Return the momentum dependent eigenenergy, formulated in the
        coordinate system with kz || 001
        """

        return [self.eps0 + self.t*(2.*(np.cos(self.a*kx)+ np.cos(self.a/2.*(kx - np.sqrt(3.)*ky))
                +np.cos(self.a/2.*(kx+np.sqrt(3.)*ky)) + np.cos(self.a/np.sqrt(3.)*(ky-np.sqrt(2.)*kz))
                + np.cos(self.a/6.*(3.*kx-np.sqrt(3.)*ky-2.*np.sqrt(6.)*kz)) + np.cos(self.a/6.*(3.*kx + np.sqrt(3.)*ky
                +2.*np.sqrt(6.)*kz))
                    ))]


    def params_str(self):
        pstr = []
        pstr.append("{}={:.2f}{}".format("model_name",self.model_name,os.linesep))
        pstr.append("{}={:.2f}{}".format("dim",self.dim,os.linesep))

        pstr.append("{}={:.2f}{}".format("t",self.t,os.linesep))
        pstr.append("{}={:.2f}{}".format("a",self.a,os.linesep))
        pstr.append("{}={:.2f}{}".format("eps0",self.eps0,os.linesep))
        return "".join(pstr)


class TightBindingCuO2(EVC):
    def __init__(self,t1=0.5,t2=0.5,a=0.41,eps0=-1):
        """
        A single band tight binding model of Cu02 as described in ...
        """
        self.dim = 1
        self.model_name = "TightBindingCu02"
        self.t1 = t1
        self.t2 = t2
        self.a = a
        self.eps0 = eps0

    def eigenenergies(self,kx,ky,kz):
        return [self.eps0-2.*self.t1*(np.cos(self.a*kx) + np.cos(self.a*ky))
                + 4.*self.t2*np.cos(self.a*kx)*np.cos(self.a*ky)]


    def params_str(self):
        pstr = []
        pstr.append("{}={:.2f}{}".format("model_name",self.model_name,os.linesep))
        pstr.append("{}={:.2f}{}".format("dim",self.dim,os.linesep))

        pstr.append("{}={:.2f}{}".format("t1",self.t1,os.linesep))
        pstr.append("{}={:.2f}{}".format("t2",self.t2,os.linesep))
        pstr.append("{}={:.2f}{}".format("a",self.a,os.linesep))
        pstr.append("{}={:.2f}{}".format("eps0",self.eps0,os.linesep))
        return "".join(pstr)



class TightBindingPeroskite(EVC):
    def __init__(self,t=0.5,a=0.41,eps0=-1):
        """
        A two band tight binding model for Peroskites as described in ...
        """
        self.dim = 2
        self.model_name = "TightBindingPerovskite"
        self.t = t
        self.a = a
        self.eps0 = eps0

    def eigenenergies(self,kx,ky,kz):
        return [self.eps0+2.*self.t*np.sqrt(np.cos(self.a*kx*0.5)**2
                    + np.cos(self.a*ky*0.5)**2 + np.cos(self.a*kz*0.5)**2),
                self.eps0-2.*self.t*np.sqrt(np.cos(self.a*kx*0.5)**2
                    + np.cos(self.a*ky*0.5)**2 + np.cos(self.a*kz*0.5)**2)]

    def params_str(self):
        pstr = []
        pstr.append("{}={:.2f}{}".format("model_name",self.model_name,os.linesep))
        pstr.append("{}={:.2f}{}".format("dim",self.dim,os.linesep))

        pstr.append("{}={:.2f}{}".format("t",self.t,os.linesep))
        pstr.append("{}={:.2f}{}".format("a",self.a,os.linesep))
        pstr.append("{}={:.2f}{}".format("eps0",self.eps0,os.linesep))
        return "".join(pstr)



class TightBindingGraphene(EVC):
    def __init__(self,a=0.426,s0=0.129,t0=1.,eps0=0.):
        """
        A two band tight binding model of Graphene as described in ...
        """
        #self.alpha =  0.0381
        self.dim = 2
        self.model_name = "TightBindingGraphene"

        self.a = a
        self.s0 = s0
        self.t0 = t0
        self.eps0 = eps0

    def eigenenergies(self,kx,ky,kz):
        fk = np.exp(1j*ky*self.a/np.sqrt(3.)) \
            + 2.*np.exp(-1j*ky*self.a*0.5/np.sqrt(3.))*np.cos(kx*self.a*0.5)

        return [(self.eps0+self.t0*np.abs(fk))/(1.-self.s0*np.abs(fk)),
                (self.eps0-self.t0*np.abs(fk))/(1.+self.s0*np.abs(fk))]

    def params_str(self):
        pstr = []
        pstr.append("{}={:.2f}{}".format("model_name",self.model_name,os.linesep))
        pstr.append("{}={:.2f}{}".format("dim",self.dim,os.linesep))

        pstr.append("{}={:.2f}{}".format("s0",self.s0,os.linesep))
        pstr.append("{}={:.2f}{}".format("t0",self.t0,os.linesep))
        pstr.append("{}={:.2f}{}".format("a",self.a,os.linesep))
        pstr.append("{}={:.2f}{}".format("eps0",self.eps0,os.linesep))
        return "".join(pstr)
