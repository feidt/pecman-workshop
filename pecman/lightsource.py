# This file is part of PECMAN

# PECMAN is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from abc import ABCMeta, abstractmethod
import cmath

class LightSource(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def value(self,w):
        """
        Return the field strength inside the solid
        """
        raise NotImplementedError("Should implement value()!")



class CWSource(LightSource):

    def __init__(self,field_strength=1.,central_energy=21.2):
        self.field_strength = field_strength
        self.central_energy = central_energy

    def value(self,w):
        return self.field_strength


class Laser(LightSource):
    def __init__(self,field_strength=1.,central_energy=21.2,spectral_width=0.02):
        self.field_strength = field_strength
        self.central_energy = central_energy
        self.spectral_width = spectral_width
        #sech: dw(in eV) = 2hbar/(pi*dt)

    def value(self,w):
        #return np.exp(-0.5*(w-self.central_energy)**2/(self.spectral_width**2))
        return 1./np.cosh((w-self.central_energy)/self.spectral_width)


""" TO DO """
class HHComb(LightSource):
    def __init__(self,field_strength=1.,central_energy=21.2):
        self.field_strength = field_strength
        self.central_energy = central_energy

    def value(self,w):
        return self.field_strength
