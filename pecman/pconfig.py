# This file is part of PECMAN

# PECMAN is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

FIG_DIM_X = 4
FIG_DIM_Y = 4
YTICK_SIZE = 16
XTICK_SIZE = 16
LABEL_SIZE = 15
LABEL_WEIGHT = 'medium'
TITLE_WEIGHT = 'book'
TITLE_SIZE = 20
CB_TICK_SIZE = 14
CB_MAX_BINS = 3
CB_LABEL_SIZE = 13
CB_LABEL_WEIGHT = 'medium'
TP_FLAG = False #transparency flag for saved images
BOUNDARY_COLOR = 'lightgreen'
EFERMI_COLOR = 'orangered'
