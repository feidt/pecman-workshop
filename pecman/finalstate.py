# This file is part of PECMAN

# PECMAN is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from abc import ABCMeta, abstractmethod
import cmath

class FinalState(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def get_kv(self,kx,ky,w):
        """
        Return the perpendicular momentum component
        in vacuum
        """
        raise NotImplementedError("Should implement get_kv()!")

    @abstractmethod
    def get_ks(self,kx,ky,w):
        """
        Return the perpendicular momentum component
        inside the solid
        """
        raise NotImplementedError("Should implement get_ks()")


class ILEEDState(FinalState):

    #check if np or cmath is needed for sqrt!!!
    def __init__(self,inner_potential):
        """Create an time-reversed LEED state"""
        self.inner_potential = inner_potential
        self.alpha =  0.0381 #hbar*hbar/2m_electron, [eV nm**2]

    def get_kv(self,kx,ky,w):
        # is w=ekin or ebin?, why cmath and not numpy, speed?,
        """TO DO: self_energy corrections, mean free path """
        kperp = cmath.sqrt(1./self.alpha*(w-self.alpha*(kx**2+ky**2)))
        return kperp

    def get_ks(self,kx,ky,w):
        kperp = cmath.sqrt(1./self.alpha*(w-self.alpha*(kx**2+ky**2)+self.inner_potential))
        return kperp
