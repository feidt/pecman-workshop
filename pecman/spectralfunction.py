# This file is part of PECMAN

# PECMAN is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import cmath
import pecman.quaternion as quat
import pecman.vector3d as vec
from pecman.selfenergy import Selfenergy
from pecman.phelper import *

class SpectralFunction(object):
    def __init__(self,evc,gamma,surface='',imagkz=1.,selfenergy=None):
        """
        Create a spectral function A(w,k) as part of the lesser Greens
        function, representing the initial state

        Attributes:
        -----------------------------------------------------------------------
        evc (EVC):      an eigenvalue calculator for band calculations
        gamma (float):  a constant spectral width of the initial states
                        (=inverse lifetime of the photohole)
        surface (str):  surface orientation of the crystal structure,
                        for fcc, e.g. 001,110,111,...
        imagkz (float): a multiplication factor for the kz value, if complex,
                        the spectral function becomes 2D, EXPERIMENTAL!!!!
                        imagkz=1 -> nothing happens
        selfenergy
        (Selfenergy):   selfenergy corrections for the initial state
        """
        self.evc = evc
        self.gamma = gamma
        self.surface = surface
        self.imagkz = imagkz
        self.selfenergy = selfenergy

        # experimental
        self.rotation_matrix = np.zeros((4,4),dtype=complex)


    def value(self,kx,ky,kz,w):
        """
        Return the total sum of the spectral function of the photohole

        To do: introduce weights for the different states
        """
        kxn, kyn, kzn = align_surface(kx,ky,kz*self.imagkz,self.surface)
        energyvalues = self.evc.eigenenergies(kxn,kyn,kzn)

        spec = 0.
        for energy in energyvalues[:]:
            spec += np.abs(np.imag(self.get_selfenergy(kx,ky,kz,w)))/((w-energy-np.real(self.get_selfenergy(kx,ky,kz,w)))**2 + np.imag(self.get_selfenergy(kx,ky,kz,w))**2)
        return spec


    """ !!!EXPERIMENTAL!!! """
    def value_ped(self,kx,ky,kz,w,surface,theta,phi):
        """
        return the total sum of the spectral function of the photohole
        """

        kxn, kyn, kzn = align_surface(kx,ky,kz,surface)
        current_vector = vec.Vector3D(np.real(kxn),np.real(kyn),np.real(kzn))
        detector_pos = vec.Vector3D(np.sin(theta)*np.cos(phi),np.sin(theta)*np.sin(phi),np.cos(theta))
        axis = current_vector.cross(detector_pos)
        angle = cmath.acos(current_vector.dot(detector_pos)/current_vector.length())
        current_quat = quat.Quaternion().build_rotation(axis,angle)
        prev_quat = quat.Quaternion()
        prev_quat.i.x = kxn
        prev_quat.i.y = kyn
        prev_quat.i.z = kzn

        current_quat.mul(prev_quat)
        current_quat.normalize()
        self.rotation_matrix = current_quat.to_matrix()
        rot_momentum_vec = self.rotation_matrix.dot(np.array([kxn,kyn,kzn,0.],dtype=complex))
        kxn, kyn, kzn = rot_momentum_vec[0], rot_momentum_vec[1], rot_momentum_vec[2]


        #kxn, kyn, kzn =
        energyvalues = self.evc.eigenenergies(kxn,kyn,kzn)

        spec = 0.
        for energy in energyvalues[:]:
            spec += np.imag(self.get_selfenergy(kx,ky,kz,w))/((w-energy-np.real(self.get_selfenergy(kx,ky,kz,w)))**2 + np.imag(self.get_selfenergy(kx,ky,kz,w))**2)
        return spec


    def get_selfenergy(self,kx,ky,kz,w):
        if isinstance(self.selfenergy,Selfenergy):
            selfenergy = self.gamma*1j + self.selfenergy.value(kx,ky,kz,w)
        else:
            selfenergy = self.gamma*1j
        return selfenergy



    @staticmethod
    def detector_rot_x(kx,ky,kz,angle):
        return kx*np.cos(angle)+kz*np.sin(angle), ky, -kx*np.sin(angle)+kz*np.cos(angle)

    #@staticmethod
    #def polar_rot(kx,ky,angle):
    #  return kx*np.cos(angle)-ky*np.sin(angle), kx*np.sin(angle)+ky*np.cos(angle)

    @staticmethod
    def detector_rot_y(kx,ky,kz,angle):
        return kx, ky*np.cos(angle)-kz*np.sin(angle), ky*np.sin(angle)+kz*np.cos(angle)


    @property
    def gamma(self):
        return self._gamma

    @gamma.setter
    def gamma(self,gamma):
        if type(gamma) is complex:
            raise ValueError("gamma has to be a positive non-complex float!")
        if gamma <= 0:
            raise ValueError("gamma has to be a positive!")
        else:
            self._gamma = gamma
