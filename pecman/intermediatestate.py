# This file is part of PECMAN

# PECMAN is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from abc import ABCMeta, abstractmethod
import cmath
from pecman.selfenergy import Selfenergy
from pecman.phelper import *

class IntermediateState(object):
    def __init__(self,evc,gamma,surface='',imagkz=1.,selfenergy=None):
        """
        Create an intermediate state object, representing the unoccupied
        part of the bandstructure

        Attributes:
        -----------------------------------------------------------------------
        evc (EVC):      an eigenvalue calculator for band calculations
        gamma (float):  a constant spectral width (=inverse lifetime)
        surface (str):  surface orientation of the crystal structure,
                        for fcc, e.g. 001,110,111,...
        imagkz (float): a multiplication factor for the kz value, if complex,
                        the state becomes 2D, EXPERIMENTAL!!!!
                        imagkz=1 -> nothing happens
        selfenergy
        (Selfenergy):   selfenergy corrections for the intermediate state
        """
        self.evc = evc
        self.gamma = gamma
        self.surface = surface
        self.imagkz = imagkz
        self.selfenergy = selfenergy


    def value(self,kx,ky,kz,w):
        """Return the retarded propagator of the intermediate state"""
        # check where to place imagkz and if this makes sense at all
        kxn, kyn, kzn = align_surface(kx,ky,kz*self.imagkz,self.surface)
        energyvalues = self.evc.eigenenergies(kxn,kyn,kzn)
        spec = 0.
        """ TO DO: introduce dipole matrix elements for intermediate states to weight the transitions"""
        for energy in energyvalues[:]:
            se = self.get_selfenergy(kx,ky,kz*self.imagkz,w)
            spec += 1./(w-energy-np.real(se) + 1j*np.imag(se))
        return spec

    def get_selfenergy(self,kx,ky,kz,w):
        if isinstance(self.selfenergy,Selfenergy):
            selfenergy = self.gamma*1j + self.selfenergy.value(kx,ky,kz,w)
        else:
            selfenergy = self.gamma*1j
        return selfenergy


    @property
    def gamma(self):
        return self._gamma

    @gamma.setter
    def gamma(self,gamma):
        if type(gamma) is complex:
            raise ValueError("gamma has to be a positive non-complex float!")
        if gamma <= 0:
            raise ValueError("gamma has to be a positive!")
        else:
            self._gamma = gamma
